/**
 * 
 */
package model.algorithm;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Iterator;

import model.Fn;
import model.GeneralConstants;
import model.Hexagon;
import model.Random;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Move.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * 
 */
public class Move implements GeneralConstants {
	private boolean _calculated = false;

	private boolean _attack = false;

	private ArrayList _children = new ArrayList();

	private final Hexagon _hexagon;

	private final int _soldier;

	private int _value;

	private int _bestValue;

	/**
	 * @param soldier
	 * @param hexagon
	 * @param value
	 */
	public Move(int soldier, Hexagon hexagon) {
		super();
		_soldier = soldier;
		_hexagon = hexagon;
	}

	public final void setValue(int value) {
		_value = value;
		_bestValue = value;
	}

	public final Move add(int soldier, Hexagon hexagon) {
		Move move = new Move(soldier, hexagon);
		_children.add(move);
		return move;
	}

	/**
	 * @return Returns the hexagon.
	 */
	public final Hexagon getHexagon() {
		return _hexagon;
	}

	/**
	 * @return Returns the soldier.
	 */
	public final int getSoldier() {
		return _soldier;
	}

	/**
	 * @return Returns the value.
	 */
	public final int getValue() {
		return _value;
	}

	public boolean hasChildren() {
		return _children.size() > 0;
	}

	/**
	 * @return Returns the calculated.
	 */
	public final boolean isCalculated() {
		return _calculated;
	}

	/**
	 * @param calculated
	 *            The calculated to set.
	 */
	public final void setCalculated(boolean calculated) {
		_calculated = calculated;
	}

	/**
	 * @return
	 */
	public Iterator getChildren() {
		return _children.iterator();
	}

	public void getAttack(AttackList attackList) {
		_bestValue = finalPrune();
		_collectAttack(attackList);
		attackList.setValue(_bestValue);
	}

	/**
	 * @param attackList
	 */
	private void _collectAttack(AttackList attackList) {
		if (!_attack) {
			// System.out.println("Collecting defence move: "+toString());
		}
		if (_children.size() > 0) {
			Move child = (Move) _children.get(0);
			attackList.add(child.getSoldier(), child.getHexagon());
			child._collectAttack(attackList);
		}
	}

	public final int finalPrune() {
		return finalPrune(0);
	}

	public final int finalPrune(int d) {
		int best = _value;
		if (_children.size() == 0) {
			// no action needed
		} else if (_children.size() == 1) {
			Move child = (Move) _children.get(0);
			int childValue = child.finalPrune(d + 1);
			if (childValue <= best) {
				_children.clear();
			} else {
				best = childValue;
			}
		} else {
			ArrayList oldChildren = _children;
			_children = new ArrayList();
			while (oldChildren.size() > 0) {
				Move child = (Move) oldChildren.remove(0);
				int childValue = child.finalPrune(d + 1);
				if (childValue > best) {
					best = childValue;
					_children.clear();
					_children.add(child);
				} else if (childValue == best && childValue > _value) {
					// only bother with equal children if greater than parent
					_children.add(child);
				}
			}
			if (_children.size() > 1) {
				Object obj = Random.get(_children);
				_children.clear();
				_children.add(obj);
			}
		}
		_bestValue = best;
		return best;
	}

	public final int getAttackSize() {
		int size = 0;
		if (_children.isEmpty()) {
			size = 1;
		} else {
			Move child = (Move) _children.get(0);
			size += child.getAttackSize();
		}
		return size;
	}

	public final int prune(int width) {
		int best = _value;
		if (_children.size() == 0) {
			// no action needed
		} else if (_children.size() == 1) {
			Move child = (Move) _children.get(0);
			int childValue = child.prune(width);
			if (childValue <= best) {
				_children.clear();
			} else {
				best = childValue;
			}
		} else {
			ArrayList oldChildren = _children;
			ArrayList bestEqual = new ArrayList();
			_children = new ArrayList();
			while (oldChildren.size() > 0) {
				Move child = (Move) oldChildren.remove(0);
				int childValue = child.prune(width);
				if (childValue > best) {
					best = childValue;
					bestEqual.clear();
					bestEqual.add(child);
				} else if (childValue == best) {
					bestEqual.add(child);
				}

				// build up ordered list of children - best last
				int insertAt = -1;
				for (int i = 0; i < _children.size() && -1 == insertAt; i++) {
					Move compare = (Move) _children.get(i);
					if (childValue <= compare.getBestValue()) {
						insertAt = i;
					}
				}
				if (-1 == insertAt) {
					_children.add(child);
				} else {
					_children.add(insertAt, child);
				}

			}
			if (bestEqual.size() >= width) {
				while (bestEqual.size() > width) {
					Random.remove(bestEqual);
				}
				_children = bestEqual;
			} else {
				while (_children.size() > width) {
					_children.remove(0);
				}
			}

		}
		_bestValue = best;
		return best;
	}

	/**
	 * @return Returns the bestValue.
	 */
	public final int getBestValue() {
		return _bestValue;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		writeOn(pw, 0);
		return sw.toString();
	}

	/**
	 * @param pw
	 * @param i
	 */
	private void writeOn(PrintWriter pw, int indent) {
		for (int i = 0; i < indent; i++) {
			pw.write("  ");
		}
		if (_calculated) {
			pw.write("*");
		}
		if (NONE != _soldier) {
			pw.write(_hexagon.toShortString());
			pw.write("; ");
			pw.write(shortString[Fn.getType(_soldier)]);
		}
		pw.write(" value=");
		pw.write(Integer.toString(_value));
		pw.println();
		for (int i = 0; i < _children.size(); i++) {
			Move move = (Move) _children.get(i);
			move.writeOn(pw, indent + 1);
		}
	}

	/**
	 * 
	 */
	public void clearAllLeavesCalculatedFlags() {
		_bestValue = 0;
		_value = 0;
		if (_children.size() == 0) {
			_calculated = false;
		} else {
			for (int i = 0; i < _children.size(); i++) {
				Move move = (Move) _children.get(i);
				move.clearAllLeavesCalculatedFlags();
			}
		}
	}

	/**
	 * @return Returns the attack.
	 */
	public final boolean isAttack() {
		return _attack;
	}

	/**
	 * @param attack
	 *            The attack to set.
	 */
	public final void setAttack() {
		_attack = true;
	}

}
