/**
 * 
 */
package model.algorithm;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Inconsistent.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * 
 */
public class Inconsistent extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
	public Inconsistent() {
		super();
	}

	/**
	 * @param arg0
	 */
	public Inconsistent(String arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 */
	public Inconsistent(Throwable arg0) {
		super(arg0);
	}

	/**
	 * @param arg0
	 * @param arg1
	 */
	public Inconsistent(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

}
