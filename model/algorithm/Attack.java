package model.algorithm;

import model.Fn;
import model.GeneralConstants;
import model.Hexagon;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Attack.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class Attack implements GeneralConstants {
	private final int _soldier;
	private final Hexagon _hexagon;

	/**
	 * @param soldier
	 * @param hexagon
	 */
	public Attack(int soldier, Hexagon hexagon) {
		super();
		_soldier = soldier;
		_hexagon = hexagon;
		if (null == hexagon) {
			throw new Error("null hexagon in attack");
		}
	}

	/**
	 * Copy constructor
	 * 
	 * @param attack
	 */
	public Attack(Attack attack) {
		_soldier = attack._soldier;
		_hexagon = attack._hexagon;
	}

	public String toString() {
		return shortString[Fn.getType(_soldier)] + "-->" + _hexagon.toString();
	}

	/**
	 * @return Returns the hexagon.
	 */
	public Hexagon getHexagon() {
		return _hexagon;
	}

	/**
	 * @return Returns the soldier.
	 */
	public int getSoldier() {
		return _soldier;
	}
}
