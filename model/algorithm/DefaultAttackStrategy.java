package model.algorithm;

import java.util.ArrayList;

import model.Fn;
import model.GeneralConstants;
import model.Hexagon;
import model.Territory;

/**
 * WARNING: this class should be created for each town, for each turn....
 * 
 * @author Phill van Leersum
 * 
 */
public class DefaultAttackStrategy extends MoveTreeBuilder implements GeneralConstants {

	private int _castleValue = 5;

	public DefaultAttackStrategy() {
	}

	/**
	 * @param town
	 */
	void buyMilitary(Territory territory) {
		int privateCount = getRankCount(1);
		int captainCount = getRankCount(2);
		int majorCount = getRankCount(3);

		// TODO: this may break the game
		// Remove the soldiers from the hexagons, but leave them registered with
		// the town
		ArrayList hexes = territory.getHexagons();
		for (int i = 0; i < hexes.size(); i++) {
			Hexagon hex = (Hexagon) hexes.get(i);
			int occ = hex.getOccupant();
			if (Fn.isSoldier(occ)) {
				hex.setOccupant(NONE);
			}
		}

		// int colonelCount = barracks.getRankCount(4);;
		// int armyCost = barracks.calculatePaybill();

		// leave enough money for next go.....
		int budget = territory.size();
		Perspective perspective = getPerspective();

		Hexagon moveTo = perspective.findEmptyLocation();
		while (null != moveTo && budget >= payOfPrivate && privateCount < 3
				&& territory.getMoney() >= costOfPrivate) {
			// System.out.println("buying private");
			territory.buyMilitary(PRIVATE, moveTo);
			privateCount++;
			budget -= costOfPrivate;
			moveTo = perspective.findEmptyLocation();
		}

		if (null != moveTo && territory.getMoney() >= costOfCastle) {
			// System.out.println("Assessing need for castle");
			Hexagon loc = perspective.findCastleLocation(_castleValue);
			if (null != loc) {
				// System.out.println("Need for castle at
				// "+loc.toShortString());
				int occ = loc.getOccupant();
				if (loc.occupantIsMovable()) {
					if (territory.moveTo(occ, moveTo)) {
						loc.setOccupant(NONE);
					}
					moveTo = perspective.findEmptyLocation();
				}
				if (loc.getOccupant() == NONE) {
					territory.buyMilitary(CASTLE, loc);
				}
			}
		}

		while (null != moveTo && budget >= payOfCaptain && captainCount < 3
				&& territory.getMoney() >= costOfCaptain) {
			// System.out.println("buying captain");
			territory.buyMilitary(CAPTAIN, moveTo);
			captainCount++;
			budget -= payOfCaptain;
			moveTo = perspective.findEmptyLocation();
		}

		while (null != moveTo && budget > payOfMajor && majorCount < 1
				&& territory.getMoney() >= costOfMajor) {
			territory.buyMilitary(MAJOR, moveTo);
			majorCount++;
			budget -= costOfMajor;
			moveTo = perspective.findEmptyLocation();
		}
	}

	/**
	 * @return Returns the castleValue.
	 */
	public final int getCastleValue() {
		return _castleValue;
	}

	/**
	 * @param castleValue
	 *            The castleValue to set.
	 */
	public final void setCastleValue(int castleValue) {
		_castleValue = castleValue;
	}
}
