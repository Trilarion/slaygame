/**
 * 
 */
package model.algorithm;

import java.util.ArrayList;
import java.util.Iterator;

import model.Fn;
import model.GeneralConstants;
import model.Hexagon;
import model.Player;
import model.Random;
import model.Territory;

/**
 * Represents a computer players view of the world. Physical data (land/sea or
 * neighbour relationships) are derived from the map but land ownership and
 * occupancy are represented in the perspective.
 * 
 * This allows perspective to be modified for speculative calculations.
 * 
 * @author Phill van Leersum
 * 
 */
public class Perspective implements GeneralConstants {

	/**
	 * Value of a boundary between two of our own hexes. Increasing this will
	 * encourage circular teritories(?).
	 */
	private static final int INTERNAL_BOUNDARY_VALUE = 2;

	private final ArrayList _enemies;

	private final Player _player;

	private int _safeEdges = 0;

	private final ArrayList _territory;

	/**
	 * @param town
	 */
	public Perspective(Territory territory) {
		super();
		_territory = new ArrayList(territory.getHexagons());
		_enemies = new ArrayList();
		_player = territory.getOwner();
		_build();
	}

	private final void _addEnemy(Hexagon hexagon) {
		_enemies.add(hexagon);
	}

	private final void _addTerritory(Hexagon hexagon) {
		_territory.add(hexagon);
	}

	/**
	 * Build initial data structures.
	 * 
	 * Assumes a discrete territory. Cannot be used when amalgamating two
	 * territories.
	 * 
	 */
	private final void _build() {
		// create player data
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData playerData = new PlayerData(_territory);
			hex.setPlayerData(playerData);
			int occ = hex.getOccupant();
			if (Fn.isCastle(occ)) {
				playerData.hasCastle = true;
			} else if (Fn.isTown(occ)) {
				playerData.hasTown = true;
			}
		}

		// fill in essential data
		hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData playerData = (PlayerData) hex.getPlayerData();

			playerData.vulnerableEdges = 0;
			Hexagon[] neighbours = hex.getNeighbours();
			for (int i = 0; i < neighbours.length; i++) {
				Hexagon neighbour = neighbours[i];
				if (null == neighbour || neighbour.isSea()) {
					_safeEdges++;
				} else {
					PlayerData neighbourData = (PlayerData) neighbour.getPlayerData();
					if (null == neighbourData) {
						// neighbour is enemy
						playerData.vulnerableEdges++;
						if (!_enemies.contains(neighbour)) {
							_enemies.add(neighbour);
						}
					} else {
						if (neighbourData.hasCastle) {
							playerData.protectedByCastle = true;
						}
						_safeEdges += INTERNAL_BOUNDARY_VALUE;
					}
				}
			}
		}
	}

	private final int _defenceScore(Hexagon hex) {
		int score = 0;
		int effectiveRank = 0;
		PlayerData playerData = (PlayerData) hex.getPlayerData();
		if (playerData.protectedByCastle) {
			effectiveRank = 2;
		} else if (playerData.hasTown) {
			effectiveRank = 1;
		}
		if (playerData.soldier != NONE) {
			int r = rankOf[Fn.getType(playerData.soldier)];
			if (r > effectiveRank) {
				effectiveRank = r;
			}
		}

		Hexagon[] neighbours = hex.getNeighbours();
		for (int i = 0; i < neighbours.length; i++) {
			Hexagon neighbour = neighbours[i];
			if (null == neighbour || neighbour.isSea()) {
			} else {
				PlayerData neighbourData = (PlayerData) neighbour.getPlayerData();

				if (null != neighbourData) {
					if (NONE != neighbourData.soldier) {
						int r = rankOf[Fn.getType(neighbourData.soldier)];
						if (r > effectiveRank) {
							effectiveRank = r;
						}
					}
				}
			}
		}

		score = (effectiveRank * playerData.vulnerableEdges);

		return score;
		// return 0;
	}

	private final boolean _hasCastle(Hexagon hexagon) {
		boolean hasCastle = false;
		int occ = hexagon.getOccupant();
		hasCastle = Fn.isCastle(occ);
		return hasCastle;
	}

	/**
	 * @param possiblyAdjacent
	 * @return
	 */
	private boolean _isAdjacentToOurTerritory(Hexagon possiblyAdjacent) {
		boolean isAdjacent = false;
		Hexagon[] neighbours = possiblyAdjacent.getNeighbours();
		for (int j = 0; j < neighbours.length && !isAdjacent; j++) {
			Hexagon maybeOurs = neighbours[j];
			if (null != maybeOurs) {
				Object obj = maybeOurs.getPlayerData();
				if (null != obj) {
					PlayerData maybeOurData = (PlayerData) obj;
					isAdjacent = maybeOurData.territory == _territory;
				}
			}
		}
		return isAdjacent;
	}

	private final void _removeEnemy(Hexagon hexagon) {
		_enemies.remove(hexagon);
	}

	private final void _removeTerritory(Hexagon hexagon) {
		_territory.remove(hexagon);
	}

	/**
	 * Change the perspective to reflect a speculative capture of a hexagon
	 * 
	 * @param capture
	 */
	public final void capture(Hexagon capture, int soldier) {
		if (!_enemies.contains(capture)) {
			throw new Inconsistent("Cannot capture - not an enemy: " + capture);
		}
		_removeEnemy(capture);
		_addTerritory(capture);
		PlayerData playerData = new PlayerData(_territory);
		capture.setPlayerData(playerData);
		playerData.soldier = soldier;
		playerData.vulnerableEdges = 0;

		Hexagon[] neighbours = capture.getNeighbours();
		for (int i = 0; i < neighbours.length; i++) {
			Hexagon neighbour = neighbours[i];
			if (null == neighbour || neighbour.isSea()) {
				_safeEdges++;
			} else {
				PlayerData neighbourData = (PlayerData) neighbour.getPlayerData();

				if (null == neighbourData) {
					// not initialized so not part of our territory.
					if (neighbour.getOwner() == _player.getId()) {
						// one of ours
						playerData.protectedByCastle = _hasCastle(neighbour);
					} else {
						// neighbour is enemy
						playerData.vulnerableEdges++;
						if (neighbour.isLand() && !_enemies.contains(neighbour)) {
							// System.out.println("Adding enemy: "+ neighbour);
							_enemies.add(neighbour);
						}
					}
				} else {
					if (neighbourData.hasCastle) {
						playerData.protectedByCastle = true;
					}
					neighbourData.vulnerableEdges--;
					_safeEdges += INTERNAL_BOUNDARY_VALUE;
				}
			}
		}
	}

	public final int defenceScore() {
		int score = 0;
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			score += _defenceScore(hex);
		}
		return score;
	}

	/**
	 * @param examine
	 * @param soldier
	 */
	public void defend(Hexagon examine, int soldier) {
		PlayerData playerData = (PlayerData) examine.getPlayerData();
		playerData.soldier = soldier;
	}

	public Hexagon findCastleLocation(int threshhold) {
		ArrayList locations = new ArrayList();
		int bestValue = threshhold;
		// 
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData ass = (PlayerData) hex.getPlayerData();
			if (!ass.protectedByCastle) {
				// only bother with those not already protected by castle
				int value = 0;
				Hexagon[] neighbours = hex.getNeighbours();
				for (int i = 0; i < neighbours.length; i++) {
					Hexagon neighbour = neighbours[i];
					if (null != neighbour) {
						PlayerData neighbour_ass = (PlayerData) neighbour.getPlayerData();
						if (null != neighbour_ass && !neighbour_ass.protectedByCastle) {
							// neighbour is one of ours and would benefit from
							// castle
							value += ass.vulnerableEdges;
						}
					}
				}
				// System.out.println("Castle value = " + value);
				if (value == bestValue) {
					locations.add(hex);
				} else if (value > bestValue) {
					locations.clear();
					locations.add(hex);
					bestValue = value;
				}
			}
		}
		return (Hexagon) Random.get(locations);
	}

	/**
	 * Assumes moves have already been made - uses hexagon.getOccupant()
	 * 
	 * @return
	 */
	public Hexagon findWeakestEmptyLocation() {
		ArrayList locations = new ArrayList();
		int bestValue = 0;
		// 
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData ass = (PlayerData) hex.getPlayerData();
			if (!ass.protectedByCastle && hex.getOccupant() == NONE) {
				// only bother with those not already protected by castle
				int value = 0;
				Hexagon[] neighbours = hex.getNeighbours();
				for (int i = 0; i < neighbours.length; i++) {
					Hexagon neighbour = neighbours[i];
					if (null != neighbour) {
						PlayerData neighbour_ass = (PlayerData) neighbour.getPlayerData();
						if (null != neighbour_ass && !neighbour_ass.protectedByCastle) {
							// neighbour is one of ours and would benefit from
							// castle
							value += (4 * ass.vulnerableEdges) - hex.effectiveRank();
						}
					}
				}
				// System.out.println("Castle value = " + value);
				if (value == bestValue) {
					locations.add(hex);
				} else if (value > bestValue) {
					locations.clear();
					locations.add(hex);
					bestValue = value;
				}
			}
		}
		return (Hexagon) Random.get(locations);
	}

	public final Hexagon findEmptyLocation() {
		Hexagon loc = null;
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext() && null == loc) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData ass = (PlayerData) hex.getPlayerData();
			if (!ass.hasCastle && !ass.hasTown && NONE == ass.soldier) {
				loc = hex;
			}
		}
		return loc;
	}

	/**
	 * @return
	 */
	public ArrayList getEnemiesCopy() {
		return new ArrayList(_enemies);
	}

	/**
	 * @return Returns the territory.
	 */
	public final ArrayList getTerritoryCopy() {
		return new ArrayList(_territory);
	}

	/**
	 * @return the number of enemy territories on our borders
	 */
	public final int numberOfEnemyPlayers() {
		ArrayList enemyTowns = new ArrayList();
		ArrayList enemies = new ArrayList(_enemies);
		while (enemies.size() > 0) {
			Hexagon hex = (Hexagon) enemies.remove(0);
			Object territory = hex.getTerritory();
			if (null != territory) {
				if (!enemyTowns.contains(territory)) {
					enemyTowns.add(territory);
				}
			}

		}
		return enemyTowns.size();
	}

	public final int safeEdges() {
		return _safeEdges;
	}

	/**
	 * @return
	 */
	public int safeHexes() {
		int safe = 0;
		Iterator hexs = _territory.iterator();
		while (hexs.hasNext()) {
			Hexagon hex = (Hexagon) hexs.next();
			PlayerData playerData = (PlayerData) hex.getPlayerData();
			if (null != playerData && 0 == playerData.vulnerableEdges) {
				safe += 1;
			}
		}
		return safe;
	}

	/**
	 * @return
	 */
	public int territorySize() {
		return _territory.size();
	}

	/**
	 * Change the perspective to undo a speculative capture of a hexagon
	 * 
	 * @param uncapture
	 */
	public final void uncapture(Hexagon uncapture) {
		if (!_territory.contains(uncapture)) {
			throw new Inconsistent("Cannot uncapture - not ours: " + uncapture);
		}
		_addEnemy(uncapture);
		uncapture.setPlayerData(null);
		_removeTerritory(uncapture);

		Hexagon[] neighbours = uncapture.getNeighbours();
		for (int i = 0; i < neighbours.length; i++) {
			Hexagon neighbour = neighbours[i];
			if (null == neighbour || neighbour.isSea()) {
				_safeEdges--;
			} else {
				PlayerData neighbourData = (PlayerData) neighbour.getPlayerData();

				if (null == neighbourData) {
					// neighbour is enemy already
					// can we remove it from _enemy list? Only if it is not
					// adjacent
					// to a member of our territory
					boolean isAdjacent = _isAdjacentToOurTerritory(neighbour);
					if (!isAdjacent) {
						// System.out.println("Removing remote enemy:
						// "+neighbour);
						_removeEnemy(neighbour);
					}
				} else {
					neighbourData.vulnerableEdges++;
					_safeEdges -= INTERNAL_BOUNDARY_VALUE;
				}
			}
		}
	}

	/**
	 * @param examine
	 * @param soldier
	 */
	public void undefend(Hexagon examine) {
		PlayerData playerData = (PlayerData) examine.getPlayerData();
		playerData.soldier = NONE;
	}

}
