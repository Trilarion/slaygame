/**
 * 
 */
package model.algorithm;

import java.util.ArrayList;

import model.GeneralConstants;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PlayerData.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * 
 */
public class PlayerData implements GeneralConstants {
	public int vulnerableEdges;

	public boolean hasCastle = false;
	public final ArrayList territory;
	public boolean protectedByCastle = false;
	public int soldier = NONE;
	public boolean hasTown;

	/**
	 * @param territory
	 */
	public PlayerData(ArrayList territory) {
		super();
		this.territory = territory;
	}

}
