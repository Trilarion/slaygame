package model.algorithm;

import java.util.ArrayList;
import java.util.Iterator;

import model.Fn;
import model.GeneralConstants;
import model.Hexagon;
import model.Territory;

/**
 * Builds a move tree by scanning available hexes.
 * 
 * The tree scans are controlled by a depth value (D) and a width value (W) that
 * work like this:
 * 
 * while not finished - scan all possible moves and children to depth D - prune
 * each level to best W children
 * 
 * This means that we do not need to hold in memory a complete tree of all
 * possibilities (very quickly runs out of memory as combination of soldiers and
 * potential targets increase).
 * 
 * It also means that we may miss some of the best combinations if we set the
 * width and depth too low.
 * 
 * 
 * @author Phill van Leersum
 * 
 */
public class MoveTreeBuilder implements GeneralConstants {

	private static int _highestAttackCount = 0;
	private static int _highestDefendCount = 0;
	private static final int DEFAULT_TIMEOUT = 1000;
	private final int[] _barracks = new int[6];
	private int _counter = 0;
	private Perspective _perspective;
	private long _timeout = DEFAULT_TIMEOUT;

	private final Timer _timer = new Timer();

	private int _treeDepth = 5;

	private int _treeWidth = 5;

	/**
	 * When building a tree we use this list to remember the hexes we have used.
	 * This saves us pointlessly repeated combinations....
	 */
	private final ArrayList _usedHexes = new ArrayList();

	public MoveTreeBuilder() {
	}

	/**
	 * @param timeout
	 * @return
	 */
	private void _attack(Move rootMove) {
		_timer.start(_timeout);

		boolean finished = false;
		while (!finished && !_timer.check()) {
			// System.out.println("("+count++ + ") scanning tree at
			// depth="+depth);
			try {
				finished = _walkTreeAttack(rootMove, _treeDepth);
			} catch (OutOfMemoryError e) {
				System.out.println("caught memory exception");
				rootMove.prune(2);
				System.out.println("pruned tree");
				finished = _walkTreeAttack(rootMove, _treeDepth);
				System.out.println("retried successfully");
			} catch (Throwable t) {
				System.out.println("caught exception during tree walk");
				t.printStackTrace();
			}
			// System.out.println("pruning tree");
			rootMove.prune(_treeWidth);
		}
		// long time = _timer.elapsed();
		// if (time > 20) {
		// System.out.println("attack took " + time + "ms");
		// }
	}

	private final int _attackScore() {
		_counter++;
		return _perspective.safeEdges() + _perspective.safeHexes()
				- _perspective.numberOfEnemyPlayers();
	}

	private final int _defenceScore() {
		_counter++;
		return _perspective.defenceScore();
	}

	/**
	 * @param timeout
	 * @return
	 */
	private void _defend(Move rootMove) {
		_timer.start(_timeout);

		_counter = 0;
		boolean finished = false;
		while (!finished && !_timer.check()) {
			// System.out.println("("+count++ + ") scanning tree at
			// depth="+depth);
			try {
				finished = _walkTreeDefend(rootMove, _treeDepth);
			} catch (OutOfMemoryError e) {
				System.out.println("caught memory exception");
				rootMove.prune(2);
				System.out.println("pruned tree");
				finished = _walkTreeDefend(rootMove, _treeDepth);
				System.out.println("retried successfully");
			} catch (Throwable t) {
				System.out.println("caught exception during tree walk");
				t.printStackTrace();
			}
			// System.out.println("pruning tree");
			rootMove.prune(_treeWidth);
		}
		// long time = _timer.elapsed();
		// if (time > 20) {
		// System.out.println("defense took " + time + "ms");
		// }
	}

	private final void _play(Move move) {
		// first set up the perspective and barracks as they should be
		int soldier = move.getSoldier();
		if (NONE != soldier) {
			removeSoldier(soldier);
		}
		Hexagon enemy = move.getHexagon();
		if (null != enemy) {
			if (move.isAttack()) {
				_perspective.capture(enemy, soldier);
			} else {
				_perspective.defend(enemy, soldier);
			}
		}
	}

	/**
	 * @param move
	 */
	private void _unplay(Move move) {
		// replace the barracks and perspective as they were before.
		int soldier = move.getSoldier();
		Hexagon enemy = move.getHexagon();

		if (NONE != soldier) {
			addSoldier(soldier);
		}
		if (null != enemy) {
			if (move.isAttack()) {
				_perspective.uncapture(enemy);
			} else {
				_perspective.undefend(enemy);
			}
		}
	}

	/**
	 * Walk a move in the tree. This may be a move that is already calculated
	 * (we are returning to a partially calculated tree) , or a move that needs
	 * new calculating.
	 * 
	 * @param parentMove
	 * @param depth
	 * @return
	 */
	private final boolean _walkTreeAttack(Move parentMove, int depth) {
		boolean finished = true;

		// first set up the perspective and barracks as they should be
		_play(parentMove);

		if (parentMove.isCalculated()) {
			Iterator childMoves = parentMove.getChildren();
			while (childMoves.hasNext() && !_timer.check()) {
				Move move = (Move) childMoves.next();
				boolean childFinished = _walkTreeAttack(move, depth);
				if (!childFinished) {
					finished = false;
				}
			}
		} else {
			int score = _attackScore();
			parentMove.setValue(score);

			// Move has not had its children calculated, so we will calculate
			// them.

			if (depth > 1 && !_timer.check()) {
				boolean moreSoldiers = armySize() > 0;
				if (moreSoldiers) {
					finished = _walkTreeAttackCalculate(parentMove, depth);
					parentMove.setCalculated(true);
				} else {
					finished = true;
				}
			} else {
				finished = false;
			}
		}

		_unplay(parentMove);
		return finished;
	}

	/**
	 * Walk a part of thhe tree that has not been calculated. Do the calculation
	 * 
	 * @param parentMove
	 * @param depth
	 * @return
	 */
	private boolean _walkTreeAttackCalculate(Move parentMove, int depth) {
		boolean finished = true;
		ArrayList enemies = _perspective.getEnemiesCopy();
		while (enemies.size() > 0 && !_timer.check()) {
			Hexagon enemy = (Hexagon) enemies.remove(0);
			if (!_usedHexes.contains(enemy)) {
				_usedHexes.add(enemy);
				int enemyRank = enemy.effectiveRank();
				int attackingSoldier = identifyMilitary(enemyRank + 1);
				if (NONE != attackingSoldier) {
					Move move = parentMove.add(attackingSoldier, enemy);
					move.setAttack();
					boolean childFinished = _walkTreeAttack(move, depth - 1);
					if (!childFinished) {
						finished = false;
					}
				}
				_usedHexes.remove(enemy);
			}
		}
		return finished;
	}

	/**
	 * Walk a move in the tree. This may be a move that is already calculated
	 * (we are returning to a partially calculated tree) , or a move that needs
	 * new calculating.
	 * 
	 * @param parentMove
	 * @param depth
	 * @return
	 */
	private final boolean _walkTreeDefend(Move parentMove, int depth) {
		boolean finished = true;
		_play(parentMove);

		if (parentMove.isCalculated()) {
			Iterator childMoves = parentMove.getChildren();
			while (childMoves.hasNext() && !_timer.check()) {
				Move move = (Move) childMoves.next();
				boolean childFinished = _walkTreeDefend(move, depth);
				if (!childFinished) {
					finished = false;
				}
			}
		} else {
			int score = _defenceScore();
			parentMove.setValue(score);
			// System.out.println("defense move evaluated: "+ parentMove);
			if (depth > 1 && !_timer.check()) {
				boolean moreSoldiers = armySize() > 0;
				if (moreSoldiers) {
					finished = _walkTreeDefendCalculate(parentMove, depth);
					parentMove.setCalculated(true);
				} else {
					finished = true;
				}
			} else {
				finished = false;
			}
		}

		_unplay(parentMove);

		return finished;
	}

	/**
	 * Walk a part of thhe tree that has not been calculated. Do the calculation
	 * 
	 * @param parentMove
	 * @param depth
	 * @return
	 */
	private boolean _walkTreeDefendCalculate(Move parentMove, int depth) {
		boolean finished = true;
		ArrayList territory = _perspective.getTerritoryCopy();
		while (territory.size() > 0 && !_timer.check()) {
			Hexagon examine = (Hexagon) territory.remove(0);
			if (!_usedHexes.contains(examine)) {
				_usedHexes.add(examine);
				int attackingSoldier = identifyMilitary(0);

				if (NONE == attackingSoldier) {
					System.out.println("Cannot defend ");
				} else {
					Move move = parentMove.add(attackingSoldier, examine);
					// System.out.println("Adding defence move: "+ move);
					boolean childFinished = _walkTreeDefend(move, depth - 1);
					if (!childFinished) {
						finished = false;
					}
				}
				_usedHexes.remove(examine);
			}
		}
		return finished;
	}

	public final void addSoldier(int soldier) {
		if (Fn.isSoldier(soldier)) {
			int rank = rankOf[Fn.getType(soldier)];
			_barracks[rank]++;
		}
	}

	/**
	 * @return size of army currently in barracks
	 */
	public final int armySize() {
		int size = 0;
		for (int i = 0; i < _barracks.length; i++) {
			size += _barracks[i];
		}
		return size;
	}

	public final AttackList attack(Territory territory) {
		_usedHexes.clear();

		for (int i = 0; i < _barracks.length; i++) {
			_barracks[i] = 0;
		}
		ArrayList hexagons = territory.getHexagons();
		for (int i = 0; i < hexagons.size(); i++) {
			Hexagon hex = (Hexagon) hexagons.get(i);
			int occ = hex.getOccupant();
			if (Fn.isSoldier(occ)) {
				addSoldier(occ);
			}
		}

		buyMilitary(territory);

		int currValue = _attackScore(); // _evaluator.evaluate(_territory,
		// _player);
		// System.out.println("Initial score = " + currValue);
		Move rootMove = new Move(NONE, null);
		rootMove.setValue(currValue);
		rootMove.setAttack();
		_counter = 0;
		int holdBack = NONE;
		if (armySize() > 4) {
			// hold back a soldier for defense
			// System.out.println("holding back soldier");
			holdBack = identifyMilitary(0);
			removeSoldier(holdBack);
		}

		_attack(rootMove);
		if (_counter > _highestAttackCount) {
			System.out.println("Examined " + _counter + " attack combinations");
			_highestAttackCount = _counter;
		}

		/* int bestAttack = */rootMove.finalPrune();
		int attackLength = rootMove.getAttackSize();
		rootMove.clearAllLeavesCalculatedFlags();

		if (NONE != holdBack) {
			// replace soldier for defense
			addSoldier(holdBack);
		}

		_counter = 0;
		_defend(rootMove);

		if (_counter > _highestDefendCount) {
			System.out.println("Examined " + _counter + " defence combinations");
			_highestDefendCount = _counter;
		}
		int defenseLength = rootMove.getAttackSize() - attackLength;

		AttackList attack = new AttackList();
		rootMove.getAttack(attack);
		/* int bestDefense = */attack.getValue();

		if (defenseLength > 0) {
			System.out.println("AttackSize=" + attackLength + "; DefenseSize=" + defenseLength);
			System.out.println(attack.toString());
		}
		return attack;
	}

	public final void beginMilitaryAdd() {
	}

	void buyMilitary(Territory territory) {
	}

	/**
	 * @return Returns the perspective.
	 */
	public final Perspective getPerspective() {
		return _perspective;
	}

	public final int getRankCount(int rank) {
		int count = 0;
		if (rank > 0 && rank < _barracks.length) {
			count = _barracks[rank];
		}
		return count;
	}

	/**
	 * @return Returns the timeout.
	 */
	public final long getTimeout() {
		return _timeout;
	}

	/**
	 * @return Returns the treeDepth.
	 */
	public final int getTreeDepth() {
		return _treeDepth;
	}

	/**
	 * @return Returns the treeWidth.
	 */
	public final int getTreeWidth() {
		return _treeWidth;
	}

	/**
	 * @param minimumRank
	 * @return soldier not removed from barracks
	 */
	public final int identifyMilitary(int minimumRank) {
		int found = NONE;
		while (NONE == found && minimumRank < _barracks.length) {
			if (_barracks[minimumRank] > 0) {
				switch (minimumRank) {
					case rankOfPrivate:
						found = PRIVATE;
						break;
					case rankOfCaptain:
						found = CAPTAIN;
						break;
					case rankOfMajor:
						found = MAJOR;
						break;
					case rankOfColonel:
						found = COLONEL;
						break;
				}
				// found = Fn.setMoveable(found, true);
				_barracks[minimumRank]--;
			} else {
				minimumRank++;
			}
		}
		return found;
	}

	/**
	 * @param town
	 */
	public void prepareFor(Territory territory) {
		_perspective = new Perspective(territory);
	}

	public final void removeSoldier(int soldier) {
		if (Fn.isSoldier(soldier)) {
			int rank = rankOf[Fn.getType(soldier)];
			if (_barracks[rank] > 0) {
				_barracks[rank]--;
			}
		}
	}

	/**
	 * @param timeout
	 *            The timeout to set.
	 */
	public final void setTimeout(long timeout) {
		_timeout = timeout;
	}

	/**
	 * @param treeDepth
	 *            The treeDepth to set.
	 */
	public final void setTreeDepth(int treeDepth) {
		_treeDepth = treeDepth;
	}

	/**
	 * @param treeWidth
	 *            The treeWidth to set.
	 */
	public final void setTreeWidth(int treeWidth) {
		_treeWidth = treeWidth;
	}

}
