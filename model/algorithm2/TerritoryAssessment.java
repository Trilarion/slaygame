package model.algorithm2;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TerritoryAssesor.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * This class represents one players assessment of one collection of hexs
 */
final class TerritoryAssessment {
	//
	// private static final int VALUE_FRONLINE_EDGE = -1;
	// private static final int VALUE_ENEMY_PLAYER = -20;
	// private static final int VALUE_FRONTLINE_HEX = 10;
	// private static final int VALUE_SAFE_HEX = 100;
	// private static final int VALUE_UNOWNED_THREAT_HEX = 12;
	// private static final int VALUE_UNOWNED_NONTHREAT_HEX = 90;
	//
	// /** collection of hexes just beyond our border */
	// private final ArrayList _enemyHexes = new ArrayList();
	//
	// /** our hexes that have an adjacent hex we do not own */
	// private final ArrayList _frontlineHexes = new ArrayList();
	//
	// /** number of hexagon edges that border our territory */
	// private int _frontlineLength = 0;
	//
	// private final ArrayList _hexagons = new ArrayList();
	//
	// /** Our hexes that are not on the front line */
	// private final ArrayList _safeHexes = new ArrayList();
	//
	// /** our hexes that have an adjacent hex we do not own */
	// private final ArrayList _seaHexes = new ArrayList();
	//
	// /** our hexes that have an adjacent hex we do not own */
	// private final ArrayList _unownedHexes = new ArrayList();
	//
	// private int _enemyPlayerCount = 0;
	//
	// private final Player _player;
	//
	// public TerritoryAssessment(Player player) {
	// super();
	// _player = player;
	// }
	//
	// /**
	// * Draw a collection of hexes and their boundaries
	// *
	// * @param graphics
	// * @param hexs
	// */
	// private final void _drawBorder(Graphics graphics, ArrayList hexs) {
	// for (int i = 0; i < hexs.size(); i++) {
	// PlanHex hexagon = (PlanHex) hexs.get(i);
	// hexagon.drawBorder(graphics);
	// }
	// }
	//
	// final int calculate(ArrayList hexagons) {
	// _hexagons.clear();
	// _hexagons.addAll(hexagons);
	// _safeHexes.clear();
	// _enemyHexes.clear();
	// _frontlineHexes.clear();
	// _seaHexes.clear();
	// _unownedHexes.clear();
	// _frontlineLength = 0;
	//
	// // First we need to determine which unowned neighbours pose a threat.
	// for (int i = 0; i < _hexagons.size(); i++) {
	// PlanHex hex = (PlanHex) _hexagons.get(i);
	// hex.addUnownedNeighboursTo(_unownedHexes);
	// }
	// // use a copy list so
	// ArrayList unowned = new ArrayList(_unownedHexes);
	// while (unowned.size() > 0) {
	// PlanHex hex = (PlanHex) unowned.remove(0);
	// PlanTerritory pt = new PlanTerritory(hex);
	// pt.add(hex, unowned);
	// unowned.removeAll(pt.getHexagons());
	// boolean threat = pt.hasEnemiesOtherThan(_player);
	// pt.setThreat(threat);
	// }
	//
	// for (int i = 0; i < _hexagons.size(); i++) {
	// PlanHex hex = (PlanHex) _hexagons.get(i);
	// int l = hex.frontlineEdges();
	// if (0 == l) {
	// _safeHexes.add(hex);
	// } else {
	// _frontlineHexes.add(hex);
	// }
	// // hex.addUnownedNeighboursTo(_unownedHexes);
	// hex.addSeaNeighboursTo(_seaHexes);
	// hex.addEnemyNeighboursTo(_enemyHexes);
	// _frontlineLength += l;
	// }
	// ArrayList enemyPlayers = new ArrayList();
	// for (int i = 0; i < _enemyHexes.size(); i++) {
	// PlanHex hex = (PlanHex) _enemyHexes.get(i);
	// Player enemy = hex.getPlanOwner();
	// if (!enemyPlayers.contains(enemy)) {
	// enemyPlayers.add(enemy);
	// }
	// }
	// _enemyPlayerCount = enemyPlayers.size();
	// int value = value();
	// return value;
	// }
	//
	// final void drawPlan(Graphics graphics) {
	// for (int i = 0; i < _safeHexes.size(); i++) {
	// PlanHex hexagon = (PlanHex) _safeHexes.get(i);
	// hexagon.drawSafe(graphics, 0);
	// }
	//
	// for (int i = 0; i < _frontlineHexes.size(); i++) {
	// PlanHex hexagon = (PlanHex) _frontlineHexes.get(i);
	// hexagon.drawFrontline(graphics, 0);
	// }
	//
	// for (int i = 0; i < _enemyHexes.size(); i++) {
	// PlanHex hexagon = (PlanHex) _enemyHexes.get(i);
	// String str = "[E" + hexagon.getIntData() + "]";
	// hexagon.drawString(graphics, str);
	// }
	//
	// for (int i = 0; i < _seaHexes.size(); i++) {
	// PlanHex hexagon = (PlanHex) _seaHexes.get(i);
	// hexagon.draw(graphics);
	// }
	//
	// for (int i = 0; i < _unownedHexes.size(); i++) {
	// PlanHex hexagon = (PlanHex) _unownedHexes.get(i);
	// String str = "[U" + hexagon.getIntData() + "]";
	// hexagon.drawString(graphics, str);
	// }
	//
	// _drawBorder(graphics, _hexagons);
	// _drawBorder(graphics, _enemyHexes);
	// _drawBorder(graphics, _seaHexes);
	// _drawBorder(graphics, _unownedHexes);
	// }
	//
	// final ArrayList getEnemies() {
	// return _enemyHexes;
	// }
	//
	// final ArrayList getFrontlineHexes() {
	// return _frontlineHexes;
	// }
	//
	// final int getFrontlineLength() {
	// return _frontlineLength;
	// }
	//
	// final ArrayList getHexagons() {
	// return _hexagons;
	// }
	//
	// final ArrayList getSafeHexes() {
	// return _safeHexes;
	// }
	//
	// final ArrayList getSeaHexes() {
	// return _seaHexes;
	// }
	//
	// final ArrayList getUnownedHexes() {
	// return _unownedHexes;
	// }
	//
	// final void report() {
	// System.out.println("Safe Hexes: " + _safeHexes.size());
	// System.out.println("Frontline hexs: " + _frontlineHexes.size());
	// System.out.println("Enemy Players: " + _enemyHexes.size());
	// }
	//
	// final int value() {
	// // System.out.println("Safe Hexes: "+_safeHexes.size());
	// // System.out.println("Frontline hexs: " + _frontlineHexes.size());
	// // System.out.println("Enemy Players: " + _enemyHexes.size());
	//
	// int threatUnowned = 0;
	// int nonThreatUnowned = 0;
	//
	// for (int i = 0; i < _unownedHexes.size(); i++) {
	// PlanHex hex = (PlanHex) _unownedHexes.get(i);
	// PlanTerritory pt = new PlanTerritory(hex);
	// if (pt.hasEnemiesOtherThan(hex.getPlanOwner())) {
	// threatUnowned++;
	// }
	// }
	//
	// int score = _safeHexes.size() * VALUE_SAFE_HEX;
	// score += _frontlineHexes.size() * VALUE_FRONTLINE_HEX;
	// score += threatUnowned * VALUE_UNOWNED_THREAT_HEX;
	// score += nonThreatUnowned * VALUE_UNOWNED_NONTHREAT_HEX;
	// score += _enemyPlayerCount * VALUE_ENEMY_PLAYER;
	// score += _frontlineLength * VALUE_FRONLINE_EDGE;
	// // System.out.println("score: " + score);
	// return score;
	// }
}
