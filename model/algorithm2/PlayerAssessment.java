package model.algorithm2;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PlayerAssessment.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * The class where players plan their moves.
 */
public class PlayerAssessment {
	// private final Player _player;
	// //private final PlanBoard _board;
	//
	// private final ArrayList _territoryAssess = new ArrayList();
	//
	// public PlayerAssessment(Player player/*, PlanBoard board*/) {
	// super();
	// _player = player;
	// //_board = board;
	// }
	//
	// public void draw(Graphics graphics) {
	// for (int i = 0; i < _territoryAssess.size(); i++) {
	// TerritoryAssesor ta = (TerritoryAssesor) _territoryAssess.get(i);
	// ta.draw(graphics);
	// }
	// }
	//
	// public void calculate() {
	// _territoryAssess.clear();
	// // ArrayList territories = _board.getTerritories();
	// // for (int i = 0; i < territories.size(); i++) {
	// // PlanTerritory pt = (PlanTerritory) territories.get(i);
	// // if (pt.getOwner() == _player) {
	// // TerritoryAssesor ta = new TerritoryAssesor(pt);
	// // _territoryAssess.add(ta);
	// // int value = ta.calculate();
	// // System.out.println("Territory " + i + " ==> " + value);
	// // ta.calculateConquest();
	// // }
	// // }
	// }
}
