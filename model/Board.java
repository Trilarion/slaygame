package model;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Board.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * most of its methods are deliberately package private
 */
public class Board extends Fn {

	static final Board newBoard(Game game, DataInputStream dis) throws IOException {
		int radius = dis.readInt();
		int rows = dis.readInt();
		int columns = dis.readInt();
		Board board = new Board(game, rows, columns, radius);
		board._load(dis);
		return board;
	}

	static final Board newBoard(Game game, int rows, int columns, int radius) {
		return new Board(game, rows, columns, radius);
	}

	private final Game _game;
	protected final Hexagon[] _hexagon;
	private int _moving = NONE;
	private Territory _pickUpTerritory = null;
	private int _radius, _rows, _columns;
	private final ArrayList _territories = new ArrayList();

	protected Board(Game game, int rows, int columns, int radius) {
		_radius = radius;
		_rows = rows;
		_columns = columns;
		_game = game;
		Hexagon[][] _hexagonGrid = new Hexagon[rows][columns];
		_hexagon = new Hexagon[rows * columns];

		int i = 0;
		for (int row = 0; row < rows; row++) {
			for (int column = 0; column < columns; column++) {
				// we actually use a subclass with more potential for the
				// planning....
				Hexagon hexagon = new Hexagon(this, row, column);
				_hexagonGrid[row][column] = hexagon;
				_hexagon[i++] = hexagon;
			}
		}
		int diameter = _radius + _radius;
		int rowSpace = (_radius * 3) / 2;
		int y = diameter;
		int _rows = _hexagonGrid.length;
		int _columns = _hexagonGrid[0].length;
		for (int row = 0; row < _rows; row++) {
			int x = diameter;
			boolean offsetRight = false;
			// offset alternate rows
			if ((row & 1) == 1) {
				offsetRight = true;
				x = x + _radius;
			}
			for (int column = 0; column < _columns; column++) {
				Hexagon hexagon = _hexagonGrid[row][column];
				hexagon.setCoordinates(x, y, _radius);
				x = x + diameter;
			}
			for (int column = 0; column < _columns; column++) {
				Hexagon hexagon = _hexagonGrid[row][column];
				if (column > 0) {
					hexagon.setLeft(_hexagonGrid[row][column - 1]);
				}
				if (column < _columns - 1) {
					hexagon.setRight(_hexagonGrid[row][column + 1]);
				}

				if (row > 0) {
					if (offsetRight) {
						hexagon.setUpLeft(_hexagonGrid[row - 1][column]);
						if (column < _columns - 1) {
							hexagon.setUpRight(_hexagonGrid[row - 1][column + 1]);
						}
					} else {
						if (column > 0) {
							hexagon.setUpLeft(_hexagonGrid[row - 1][column - 1]);
						}
						hexagon.setUpRight(_hexagonGrid[row - 1][column]);
					}
				}

				if (row < _rows - 1) {
					if (offsetRight) {
						hexagon.setDownLeft(_hexagonGrid[row + 1][column]);
						if (column < _columns - 1) {
							hexagon.setDownRight(_hexagonGrid[row + 1][column + 1]);
						}
					} else {
						if (column > 0) {
							hexagon.setDownLeft(_hexagonGrid[row + 1][column - 1]);
						}
						hexagon.setDownRight(_hexagonGrid[row + 1][column]);
					}

				}
			}
			y = y + rowSpace;
		}
	}

	private final Hexagon _find(Point pt) {
		Hexagon found = null;
		for (int i = 0; null == found && i < _hexagon.length; i++) {
			Hexagon hexagon = _hexagon[i];
			if (hexagon.contaimsPoint(pt)) {
				found = hexagon;
				if (null != found) {
					int playerId = found.getOwner();
					if (NO_PLAYER != playerId) {
						if (getPlayer(playerId).isHuman()) {
							_setCurrentTerritory(found.getTerritory());
						}
					}
				}
			}
		}
		return found;
	}

	private final void _load(DataInputStream dis) throws IOException {
		for (int i = 0; i < _hexagon.length; i++) {
			Hexagon hexagon = _hexagon[i];
			int data = dis.readInt();
			hexagon.setState(data, _game.getPlayers());
		}
	}

	private final void _setCurrentTerritory(Territory territory) {
		for (int t = 0; t < _territories.size(); t++) {
			Territory ter = (Territory) _territories.get(t);
			ter.setCurrent(ter == territory);
		}
		_game.setCurrentTerritory(territory);
	}

	final void add(Territory territory) {
		if (!_territories.contains(territory)) {
			_territories.add(territory);
		}
	}

	final void buyMilitary(int type, Territory territory, Point pt) {
		Hexagon hexagon = _find(pt);
		if (null != hexagon && hexagon.isLand()) {
			territory.buyMilitary(type, hexagon);
		}
	}

	/**
	 * @return ArrayList of towns
	 */
	final void checkTowns() {
		_territories.clear();
		// place all land in collection
		ArrayList land = new ArrayList();
		for (int i = 0; i < _hexagon.length; i++) {
			Hexagon lookat = _hexagon[i];
			if (lookat.isLand() && NO_PLAYER != lookat.getOwner()) {
				land.add(lookat);
				lookat.setTerritory(null);
			}
		}

		// ArrayList territories = new ArrayList();

		// remove land from collection into groups
		while (land.size() > 0) {
			Hexagon lookat = (Hexagon) land.remove(0);
			if (NO_PLAYER != lookat.getOwner() && null == lookat.getTerritory()) {
				Territory territory = new Territory(this, lookat);
				if (territory.size() > 1) {
					_territories.add(territory);
					// _towns.add(territory.getTown());
				} else {
					lookat.setOccupant(NONE);
					lookat.setOwner(NO_PLAYER);
				}
			}
		}
	}

	final void clearPlayerData() {
		for (int i = 0; i < _hexagon.length; i++) {
			Hexagon hexagon = _hexagon[i];
			hexagon.setPlayerData(null);

		}
	}

	public void createNewMap(Player[] players) {
		WorldCreator worldCreator = new WorldCreator(_hexagon);
		worldCreator.create();
		worldCreator.allocateLand(players);
	}

	final void draw(Graphics graphics, boolean flashOn, Player human) {
		ArrayList drawnHexes = new ArrayList();
		for (int i = 0; i < _hexagon.length; i++) {
			Hexagon hexagon = _hexagon[i];
			boolean visible = true;
			if (Settings.expert) {
				visible = hexagon.isVisibleToHuman(human.getId());
			}
			if (visible) {
				hexagon.draw(graphics);
				drawnHexes.add(hexagon);
				hexagon.drawOccupant(graphics, flashOn);
			} else if (Settings.rememberMap && hexagon.hasBeenSeenByHuman()) {
				hexagon.drawBasic(graphics);
				drawnHexes.add(hexagon);
			}

		}

		Iterator hexes = drawnHexes.iterator();
		while (hexes.hasNext()) {
			Hexagon hex = (Hexagon) hexes.next();
			hex.drawBorder(graphics);
		}

	}

	final Player getPlayer(int id) {
		return _game.getPlayer(id);
	}

	final int[] getRankings(Player[] players) {
		int[] rankings = new int[players.length];
		for (int i = 0; i < players.length; i++) {
			Player player = players[i];
			Iterator terrs = _territories.iterator();
			while (terrs.hasNext()) {
				Territory ter = (Territory) terrs.next();
				if (ter.getOwner() == player) {
					rankings[i] += ter.size();
				}
			}
		}
		return rankings;
	}

	/**
	 * Get the towns owned by a player, set those towns as 'in play', set all
	 * other towns as not 'in play'.
	 * 
	 * @param player
	 */
	final ArrayList getTerritories(Player player) {
		ArrayList playersTerritories = new ArrayList();
		for (int t = 0; t < _territories.size(); t++) {
			Territory ter = (Territory) _territories.get(t);
			if (ter.getOwner() == player && ter.isValid()) {
				playersTerritories.add(ter);
				ter.beginTurn();
			}
		}
		return playersTerritories;
	}

	final Territory getTerritory(Point pt, Player human) {
		Territory territory = null;
		Hexagon hexagon = _find(pt);
		if (null != hexagon) {
			territory = hexagon.getTerritory();
		}
		return territory;
	}

	final Cursor pickUp(Point pt) {
		Cursor pickedUp = null;
		if (NONE == _moving) {
			Hexagon hexagon = _find(pt);
			if (null != hexagon) {
				Player player = getPlayer(hexagon.getOwner());
				if (player.isHuman()) {
					_game.setCurrentTerritory(hexagon.getTerritory());
					int occ = hexagon.getOccupant();
					if (NONE != occ && isMoveable(occ)) {
						_moving = occ;
						int type = getType(_moving);
						if (isTown(occ)) {
							if (getMoney(occ) > costOfPrivate) {
								pickedUp = Glyphs.RED_TOWN_CURSOR;
							}
							pickedUp = Glyphs.TOWN_CURSOR;
						} else if (NONE != type && type < Glyphs.cursor.length) {
							pickedUp = Glyphs.cursor[type];
						}
						_pickUpTerritory = hexagon.getTerritory();
						hexagon.setOccupant(NONE);
					}
				}
			}
		}
		return pickedUp;
	}

	final boolean putDown(Point pt) {
		boolean released = false;
		if (NONE != _moving) {
			Hexagon dstHex = _find(pt);
			if (null != dstHex && dstHex.isLand()) {
				released = _pickUpTerritory.moveTo(_moving, dstHex);
			}

			if (released) {
				_moving = NONE;
				_pickUpTerritory = null;
				_game.setCurrentTerritory(dstHex.getTerritory());
			}
		}
		return released;
	}

	final void remove(Territory territory) {
		_territories.remove(territory);
	}

	/**
	 * @param dos
	 * @throws IOException
	 */
	final void save(DataOutputStream dos) throws IOException {
		dos.writeInt(_radius);
		dos.writeInt(_rows);
		dos.writeInt(_columns);

		for (int i = 0; i < _hexagon.length; i++) {
			Hexagon hexagon = _hexagon[i];
			dos.writeInt(hexagon.getState());
		}
	}
}
