package model;

import gui.SlayPanel;

import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Iterator;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Game.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public final class Game implements GeneralConstants {
	private static final int FILE_EXPERT = 1;
	private Board _board;
	private boolean _gameOn = false;
	private final long _gameSeed; // use to reset random when restarting game
	private final Player _human;
	private int _moveNumber = 0;

	private final Player[] _players = new Player[6];
	private final SlayPanel _slayPanel;

	private Game(SlayPanel slayPanel) {
		super();

		_slayPanel = slayPanel;

		for (int i = 0; i < 6; i++) {
			Player player = new Player(i, false);
			_players[i] = player;
		}
		_players[0].setHuman(true);
		_human = _players[0];
		_gameSeed = Random.nextLong();
		Random.seed(_gameSeed);
	}

	public Game(SlayPanel slayPanel, DataInputStream dis) throws IOException {
		this(slayPanel);
		// Header
		int int1 = dis.readInt();
		/* int i2 = */dis.readInt();
		/* int i3 = */dis.readInt();
		/* int i4 = */dis.readInt();

		Settings.expert = FILE_EXPERT == int1;

		for (int i = 0; i < _players.length; i++) {
			Player player = _players[i];
			player.load(dis);
		}
		_board = Board.newBoard(this, dis);

		_board.checkTowns();
		_startGame();
	}

	public Game(SlayPanel slayPanel, int rows, int columns, int radius) {
		this(slayPanel);

		_board = Board.newBoard(this, rows, columns, radius);

		_board.createNewMap(_players);

		_board.checkTowns();
		_startGame();
	}

	private final void _checkForWinner() {
		Player winner = null;
		for (int p = 0; p < _players.length; p++) {
			Player player = _players[p];
			if (player.isInTheGame()) {
				int[] rankings = _board.getRankings(_players);
				boolean playerHasOpponent = false;
				for (int i = 0; i < rankings.length; i++) {
					int rank = rankings[i];
					if (rank == 0) {
						if (_players[i].isInTheGame()) {
							System.out.println(_players[i].getName() + " lost!");
							_players[i].setInTheGame(false);
						}
					} else if (player != _players[i]) {
						playerHasOpponent = true;
					}
				}
				if (!playerHasOpponent) {
					winner = player;
				}

				_slayPanel.setRankings(rankings);
			}
		}
		if (null != winner) {
			_slayPanel.congratulations(winner);
			_gameOn = false;
		}
	}

	private final void _playOneNonHumanTurn() {
		// human is always player 0, so start from 1
		for (int p = 1; p < _players.length; p++) {
			_board.checkTowns();
			Player player = _players[p];
			if (player.isInTheGame()) {
				// System.out.println("Player: " + p);
				// _board.clearPlayerData();
				ArrayList territories = _board.getTerritories(player);
				player.play(territories);
			}
			_checkForWinner();
		}
		// _board.checkTowns();
	}

	/**
	 * 
	 */
	private final void _startGame() {
		// System.out.println("startGame");
		_moveNumber = 1;
		for (int p = 0; p < _players.length; p++) {
			Player player = _players[p];
			player.setInTheGame(true);
		}
		_slayPanel.setMove(_moveNumber);
		_gameOn = true;
		boolean humanCanMove = _startHumanTurn();
		while (!humanCanMove) {
			_checkForWinner();
			if (_gameOn) {
				_playOneNonHumanTurn();
			}
			_slayPanel.setMove(_moveNumber);
			if (_gameOn) {
				humanCanMove = _startHumanTurn();
			}
		}
		_slayPanel.repaintViews();
		_slayPanel.enableGui();
		// System.out.println("ready for human play");
	}

	private final boolean _startHumanTurn() {
		boolean humanCanMove = false;
		if (_human.isInTheGame()) {
			_board.clearPlayerData();
			ArrayList playersTerritories = _board.getTerritories(_human);
			Iterator territories = playersTerritories.iterator();
			while (!humanCanMove && territories.hasNext()) {
				Territory territory = (Territory) territories.next();
				if (territory.getMoney() >= 10) {
					humanCanMove = true;
				} else if (territory.hasArmy()) {
					humanCanMove = true;
				}
			}
			_slayPanel.setMove(_moveNumber++);
		}
		if (humanCanMove) {
			_human.setPlaying(true);
		}
		return humanCanMove;
	}

	public final void buyCaptain(Territory territory, Point pt) {
		_board.buyMilitary(CAPTAIN, territory, pt);
	}

	public final void buyCastle(Territory territory, Point pt) {
		_board.buyMilitary(CASTLE, territory, pt);
	}

	public final void buyColonel(Territory territory, Point pt) {
		_board.buyMilitary(COLONEL, territory, pt);
	}

	public final void buyMajor(Territory territory, Point pt) {
		_board.buyMilitary(MAJOR, territory, pt);
	}

	public final void buyPrivate(Territory territory, Point pt) {
		_board.buyMilitary(PRIVATE, territory, pt);
	}

	/**
	 * 
	 */
	public final void draw(Graphics graphics, boolean flash) {
		_board.draw(graphics, flash, _human);
	}

	/**
	 * 
	 */
	public final void endHumanTurn() {
		_human.setPlaying(false);
		// System.out.println("end of human turn");
		_slayPanel.disableGUI();
		_checkForWinner();
		if (_gameOn) {
			_playOneNonHumanTurn();
		}
		_slayPanel.setMove(_moveNumber);
		if (_gameOn) {
			_startHumanTurn();
		}
		_slayPanel.repaintViews();
		_slayPanel.enableGui();
		// System.out.println("ready for human turn");
	}

	final Player getPlayer(int id) {
		return _players[id];
	}

	public final Player[] getPlayers() {
		return _players;
	}

	public final Territory getTerritory(Point pt) {
		return _board.getTerritory(pt, _human);
	}

	public final Cursor pickUp(Point pt) {
		return _board.pickUp(pt);
	}

	public final boolean putDown(Point pt) {
		return _board.putDown(pt);
	}

	public final void restart() {
		Random.seed(_gameSeed);
		_board.createNewMap(_players);
		_board.checkTowns();
		_slayPanel.repaintViews();
		_startGame();
	}

	public final void save(File file) throws IOException {
		if (null != file) {
			OutputStream os = new FileOutputStream(file);
			DataOutputStream dos = new DataOutputStream(os);
			int int1 = 0;
			if (Settings.expert) {
				int1 = FILE_EXPERT;
			}
			// header
			dos.writeInt(int1);
			dos.writeInt(-1);
			dos.writeInt(-1);
			dos.writeInt(-1);

			for (int i = 0; i < _players.length; i++) {
				Player player = _players[i];
				player.save(dos);
			}
			_board.save(dos);

			dos.close();
		}
	}

	final void setCurrentTerritory(Territory territory) {
		_slayPanel.setCurrentTerritory(territory);
	}
}
