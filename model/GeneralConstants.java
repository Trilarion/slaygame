package model;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * MilitaryConstants.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * DO NOT SORT THIS CLASS!
 */
public interface GeneralConstants {
	public static final int NO_PLAYER = -1;

	public static final int NONE = 0x0F;
	public static final int PRIVATE = 0;
	public static final int CAPTAIN = 1;
	public static final int MAJOR = 2;
	public static final int COLONEL = 3;
	public static final int CASTLE = 4;
	public static final int MILITARY_TYPES = 5;
	public static final int TOWN = 5;
	public static final int GRAVE = 6;

	public static final int costOfCaptain = 20;
	public static final int costOfCastle = 20;
	public static final int costOfColonel = 40;
	public static final int costOfMajor = 30;
	public static final int costOfPrivate = 10;
	public static final int costOfPromotion = 10;

	public static final int payOfCaptain = 6;
	public static final int payOfCastle = 0;
	public static final int payOfColonel = 54;
	public static final int payOfMajor = 18;
	public static final int payOfPrivate = 2;

	public static final int rankOfPrivate = 1;
	public static final int rankOfCaptain = 2;
	public static final int rankOfCastle = 2;
	public static final int rankOfMajor = 3;
	public static final int rankOfColonel = 4;
	public static final int rankOfTown = 1;
	public static final int rankOfGrave = 0;

	public static final int[] costOf = new int[] { costOfPrivate, costOfCaptain, costOfMajor,
			costOfColonel, costOfCastle };

	public static final int[] payOf = new int[] { payOfPrivate, payOfCaptain, payOfMajor,
			payOfColonel, payOfCastle };

	public static final int[] rankOf = new int[] { rankOfPrivate, rankOfCaptain, rankOfMajor,
			rankOfColonel, rankOfCastle, rankOfTown, rankOfGrave };

	public static final String[] shortString = new String[] { "Pvt", "Cpt", "Mjr", "Col", "Cas",
			"Twn" };

}
