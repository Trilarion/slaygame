package model;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Glyphs.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Convenience class for drawing soldiers
 */
public final class Glyphs {
	public static final Color CAPTAIN_COLOUR = new Color(160, 160, 255);
	public static final Color COLONEL_COLOUR = new Color(255, 0, 255);
	public static final Color MAJOR_COLOUR = new Color(200, 100, 255);
	public static final Color PRIVATE_COLOUR = new Color(225, 225, 255);
	public static final Color[] colour = new Color[] { PRIVATE_COLOUR, CAPTAIN_COLOUR,
			MAJOR_COLOUR, COLONEL_COLOUR, Color.GRAY };

	public static final Cursor CURSOR;
	public static final Cursor CAPTAIN_CURSOR;
	public static final Cursor COLONEL_CURSOR;
	public static final Cursor CASTLE_CURSOR;
	public static final Cursor MAJOR_CURSOR;
	public static final Cursor PRIVATE_CURSOR;
	public static final Cursor TOWN_CURSOR;
	public static final Cursor RED_TOWN_CURSOR;
	public static final Cursor[] cursor;

	static {
		Toolkit tk = Toolkit.getDefaultToolkit();
		BufferedImage image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		Graphics graphics = image.getGraphics();
		drawSoldier(graphics, Color.BLACK, Color.WHITE, 16, 16, 18);
		CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawSoldier(graphics, Color.BLACK, CAPTAIN_COLOUR, 16, 16, 18);
		CAPTAIN_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawSoldier(graphics, Color.BLACK, COLONEL_COLOUR, 16, 16, 18);
		COLONEL_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawSoldier(graphics, Color.BLACK, MAJOR_COLOUR, 16, 16, 18);
		MAJOR_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawSoldier(graphics, Color.BLACK, PRIVATE_COLOUR, 16, 16, 18);
		PRIVATE_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawCastle(graphics, 16, 16, 18);
		CASTLE_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		image = new BufferedImage(32, 32, BufferedImage.TYPE_INT_ARGB);
		graphics = image.getGraphics();
		drawTown(graphics, null, Color.LIGHT_GRAY, 16, 16, 18);
		TOWN_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");
		drawTown(graphics, null, Color.RED, 16, 16, 18);
		RED_TOWN_CURSOR = tk.createCustomCursor(image, new Point(10, 10), "Soldier");

		cursor = new Cursor[] { PRIVATE_CURSOR, CAPTAIN_CURSOR, MAJOR_CURSOR, COLONEL_CURSOR,
				CASTLE_CURSOR, TOWN_CURSOR, RED_TOWN_CURSOR };
	}

	public static final void drawSoldier(Graphics graphics, Color lineColor, Color fillColour,
			int x, int y, int size) {
		Color oldColour = graphics.getColor();
		int half = size / 2;
		int quarter = half / 2;
		int eighth = quarter / 2;
		int threeQuarters = half + quarter;
		int threeEighths = quarter + eighth;
		int fiveEighths = half + eighth;

		// helmet
		graphics.setColor(fillColour);
		graphics.fillOval(x - quarter, y + eighth - size, half, threeQuarters);
		graphics.setColor(lineColor);
		graphics.drawOval(x - quarter, y + eighth - size, half, threeQuarters);

		// body
		int[] xx = new int[] { x, x + quarter, x + quarter, x + eighth, x + eighth, x + eighth,
				x + quarter, x + quarter, x, x, x, x - quarter, x - quarter, x - eighth,
				x - eighth, x - eighth, x - quarter, x - quarter };
		int[] yy = new int[] { y - threeEighths, y - eighth, y + quarter, y + quarter, y,
				y + fiveEighths, y + fiveEighths, y + threeQuarters, y + threeQuarters,
				y + quarter, y + threeQuarters, y + threeQuarters, y + fiveEighths,
				y + fiveEighths, y, y + quarter, y + quarter, y - eighth };
		graphics.setColor(fillColour);
		graphics.fillPolygon(xx, yy, 18);
		graphics.setColor(lineColor);
		graphics.drawPolygon(xx, yy, 18);

		// face
		graphics.setColor(Color.WHITE);
		graphics.fillOval(x - quarter, y + eighth - threeQuarters, half, threeEighths);
		graphics.setColor(lineColor);
		graphics.drawOval(x - quarter, y + eighth - threeQuarters, half, threeEighths);

		graphics.setColor(oldColour);
	}

	public Color fillColor;
	public Color lineColour;
	public int size;
	public int x;

	public int y;

	public Glyphs(int x, int y, int size) {
		super();
		this.x = x;
		this.y = y;
		this.size = size;
	}

	public final void drawCaptain(Graphics graphics) {
		drawSoldier(graphics, Color.BLACK, CAPTAIN_COLOUR, x, y, size);
	}

	public final void drawColonel(Graphics graphics) {
		drawSoldier(graphics, Color.BLACK, COLONEL_COLOUR, x, y, size);
	}

	public final void drawMajor(Graphics graphics) {
		drawSoldier(graphics, Color.BLACK, MAJOR_COLOUR, x, y, size);
	}

	public final void drawPaleSoldier(Graphics graphics) {
		drawSoldier(graphics, Color.LIGHT_GRAY, PRIVATE_COLOUR, x, y, size);
	}

	public final void drawPrivate(Graphics graphics) {
		drawSoldier(graphics, Color.BLACK, PRIVATE_COLOUR, x, y, size);
	}

	public static void drawGrave(Graphics graphics, int x, int y, int size) {
		Color oldColour = graphics.getColor();
		int half = size / 2;
		int quarter = half / 2;
		int eighth = quarter / 2;
		int threeEighths = (half + quarter) / 2;
		int threeQuarters = half + quarter;

		// String str = "R.I.P";
		int x1 = x - threeEighths;
		int x2 = x - eighth;
		int x3 = x + eighth;
		int x4 = x + threeEighths;
		int y1 = y - threeEighths;
		int y2 = y - eighth;
		int y3 = y + eighth;
		int y4 = y + threeQuarters;

		// FontMetrics fm = graphics.getFontMetrics();

		int[] xx = new int[] { x2, x3, x3, x4, x4, x3, x3, x2, x2, x1, x1, x2 };
		int[] yy = new int[] { y1, y1, y2, y2, y3, y3, y4, y4, y3, y3, y2, y2 };
		graphics.setColor(Color.LIGHT_GRAY);
		graphics.fillPolygon(xx, yy, 12);
		graphics.setColor(Color.BLACK);
		graphics.drawPolygon(xx, yy, 12);

		graphics.setColor(oldColour);
	}

	public static void drawTown(Graphics graphics, String str, Color colour, int x, int y, int size) {
		Color oldColour = graphics.getColor();
		int half = size / 2;
		int quarter = half / 2;
		int eighth = quarter / 2;
		int threeEighths = (half + quarter) / 2;

		graphics.setColor(colour);
		graphics.fillRect(x - threeEighths, y - eighth, half + quarter, half + eighth);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(x - threeEighths, y - eighth, half + quarter, half + eighth);
		graphics.setColor(oldColour);

		int[] xx = new int[] { x - half, x + half, x + quarter, x - quarter };
		int[] yy = new int[] { y - eighth, y - eighth, y - half, y - half };
		graphics.setColor(colour);
		graphics.fillPolygon(xx, yy, 4);
		graphics.setColor(Color.BLACK);
		graphics.drawPolygon(xx, yy, 4);

		if (null != str) {
			FontMetrics fm = graphics.getFontMetrics();
			int width = fm.stringWidth(str);
			int height = fm.getHeight();
			int tx = x - (width / 2);
			int ty = y + eighth + (height / 2);// - fm.getAscent();
			graphics.setColor(Color.BLACK);
			graphics.drawString(str, tx, ty);
		}
	}

	public static void drawCastle(Graphics graphics, Color lineColor, Color fillColor, int x,
			int y, int size) {
		Color oldColour = graphics.getColor();
		int half = size / 2;
		int quarter = half / 2;
		int threeEighths = (half + quarter) / 2;

		int[] xx = new int[] { x - quarter, x + quarter, x + threeEighths, x - threeEighths };
		int[] yy = new int[] { y - quarter, y - quarter, y + half, y + half };
		graphics.setColor(fillColor);
		graphics.fillPolygon(xx, yy, 4);
		graphics.setColor(lineColor);
		graphics.drawPolygon(xx, yy, 4);

		graphics.setColor(fillColor);
		graphics.fillRect(x - threeEighths, y - half, quarter + half, quarter);
		graphics.setColor(lineColor);
		graphics.drawRect(x - threeEighths, y - half, quarter + half, quarter);
		graphics.setColor(oldColour);
	}

	public static void drawCastle(Graphics graphics, int x, int y, int size) {
		drawCastle(graphics, Color.BLACK, Color.LIGHT_GRAY, x, y, size);
	}

	public static void drawPaleCastle(Graphics graphics, int x, int y, int size) {
		drawCastle(graphics, Color.LIGHT_GRAY, Color.WHITE, x, y, size);
	}

}
