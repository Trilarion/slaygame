package model;

import java.util.ArrayList;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * WorldCreator.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public final class WorldCreator implements GeneralConstants {

	private static final int FAIL_LIMIT = 100;
	private final Hexagon[] _hexagon;

	public WorldCreator(Hexagon[] hexagon) {
		_hexagon = hexagon;
	}

	private void _createLand() {
		ArrayList land = new ArrayList();

		Hexagon hexagon = _hexagon[_hexagon.length / 2];
		hexagon.setAsLand();
		land.add(hexagon);
		int fails = 0;
		System.out.println("Create Land - Phase1");
		int targetLandArea = (_hexagon.length * 2) / 10;
		while (land.size() < targetLandArea && fails++ < FAIL_LIMIT) {
			boolean changed = false;
			int tries = 0;
			while (!changed && tries++ < FAIL_LIMIT) {
				Hexagon targetLand = (Hexagon) Random.get(land);
				if (targetLand.getSeaNeighbourCount() > 2) {
					Hexagon newLand = targetLand.getNeighbour(Random.positiveInteger(6));
					if (null != newLand && newLand.isSea()) {
						newLand.setAsLand();
						changed = true;
						land.add(newLand);
					}
				}
			}
			if (tries >= FAIL_LIMIT) {
				fails++;
			}
		}
		System.out.println("Create Land - Phase2");
		fails = 0;
		targetLandArea = (_hexagon.length * 7) / 10;
		while (land.size() < targetLandArea && fails < 100) {
			boolean changed = false;
			int tries = 0;
			while (!changed && tries++ < FAIL_LIMIT) {
				Hexagon targetLand = (Hexagon) Random.get(land);
				if (targetLand.getSeaNeighbourCount() > 2) {
					Hexagon newLand = targetLand.getNeighbour(Random.positiveInteger(6));
					if (null != newLand && newLand.isSea()) {
						newLand.setAsLand();
						changed = true;
						land.add(newLand);
					}
				}
			}
			if (tries >= FAIL_LIMIT) {
				fails++;
			}
		}
		System.out.println("Created Land");
	}

	/**
	 * 
	 */
	private void _removeIslands() {
		Hexagon hexagon;
		// we can still wind up with islands - so eliminate them
		// fill collection with all land hexagons
		ArrayList hexes = new ArrayList();

		for (int row = 0; row < _hexagon.length; row++) {
			hexagon = _hexagon[row];
			if (hexagon.isLand()) {
				hexes.add(hexagon);
			}
		}
		ArrayList biggestIsland = new ArrayList();
		while (!hexes.isEmpty()) {
			ArrayList island = _getIsland(hexes);
			System.out.println("Island size = " + island.size());
			if (island.size() > biggestIsland.size()) {
				// contents of biggest island become sea
				_makeSea(biggestIsland);
				biggestIsland = island;
			} else {
				_makeSea(island);
			}
		}
	}

	private final void _makeSea(ArrayList list) {
		for (int i = 0; i < list.size(); i++) {
			Hexagon h = (Hexagon) list.get(0);
			h.setAsSea();
		}
	}

	private ArrayList _getIsland(ArrayList world) {
		ArrayList island = new ArrayList();
		if (!world.isEmpty()) {
			Hexagon hex = (Hexagon) world.remove(0);
			island.add(hex);
			_getIsland(hex, island, world);
		}
		return island;
	}

	/**
	 * @param hex
	 * @param island
	 * @param world
	 */
	private void _getIsland(Hexagon hex, ArrayList island, ArrayList world) {
		Hexagon[] hexes = hex.getNeighbours();
		for (int i = 0; i < hexes.length; i++) {
			Hexagon hexagon = hexes[i];
			if (null != hexagon && world.contains(hexagon)) {
				world.remove(hexagon);
				island.add(hexagon);
				_getIsland(hexagon, island, world);
			}

		}
	}

	private void _modifyPonds() {
		// reduce number of single-water ponds
		for (int row = 0; row < _hexagon.length; row++) {
			Hexagon lookat = _hexagon[row];
			if (lookat.isSea()) {
				if (0 == lookat.getSeaNeighbourCount()) {
					Hexagon target = lookat.getNeighbour(Random.positiveInteger(6));
					if (null != target) {
						target.setAsSea();
					}

				}
			}
		}
	}

	void _setAllToSea() {
		// first set all to sea
		for (int row = 0; row < _hexagon.length; row++) {
			Hexagon hexagon = _hexagon[row];
			// int type = random(playerCount - 1) + 1;
			// hexagon.setLand(type);
			hexagon.setAsSea();
		}

	}

	public final void allocateLand(Player[] players) {
		ArrayList land = new ArrayList();
		for (int row = 0; row < _hexagon.length; row++) {
			Hexagon lookat = _hexagon[row];
			if (lookat.isLand()) {
				land.add(lookat);
				lookat.setOccupant(NONE);

			}
		}
		int p = 0;
		while (land.size() > 0) {
			Hexagon lookat = (Hexagon) Random.remove(land);
			Player player = players[p];
			lookat.setOwner(player.getId());
			p += 1;
			if (p >= players.length) {
				p = 0;
			}
		}
	}

	public final void create() {
		for (int row = 0; row < _hexagon.length; row++) {
			_hexagon[row].reset();
			_hexagon[row]._isLand = false;
		}
		_createLand();
		_modifyPonds();
		_removeIslands();
	}
}
