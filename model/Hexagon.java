package model;

import java.awt.Color;
import java.awt.Graphics;

/**
 * 
 */
public class Hexagon extends AbstractHexagon {

	private static final int LAND = 0x80000000;
	private static final int OCCUPANT_MASK = 0x0FFFFFFF;

	private static final int PLAYER_MASK = 0x0F000000;
	private static final int PLAYER_SHIFT = 24;
	public static final Color SEA_COLOUR = new Color(180, 225, 255);
	private static final int UNOWNED = 0x0F000000;
	private static final Color UNOWNED_LAND_COLOUR = new Color(220, 220, 220);

	private boolean _hasBeenSeenByHuman = false;
	private int _occupant = NONE;
	private int _owningPlayer = NO_PLAYER;
	private Object _playerData;
	private Territory _territory = null;

	protected Hexagon(Board board, int row, int column) {
		super(board, row, column);
	}

	final void capture(Territory srcTerritory, int soldier, int newOwner) {
		if (isTown(_occupant)) {
			// direct capture of town captures money....
			int money = getMoney(_occupant);
			money += srcTerritory.getMoney();
			srcTerritory.setMoney(money);
		}

		if (NO_PLAYER == _owningPlayer) {
			_owningPlayer = newOwner;
			_occupant = soldier;
			_territory = srcTerritory;
			srcTerritory.addOneHex(this);
			// look at neighbours to see if we have amalgamated two
			// territories
		} else if (newOwner == _owningPlayer) {
			System.out.println("Capture????");
		} else {
			// need to check that territory is viable
			_owningPlayer = newOwner;
			setOccupant(soldier);
			_territory.remove(this);
			// _territory = srcTerritory;
			// _board.checkTowns();
			srcTerritory.addOneHex(this);
		}
		for (int i = 0; i < _neighbours.length; i++) {
			Hexagon neighbour = _neighbours[i];
			if (null != neighbour && neighbour.getOwner() == newOwner) {
				// the neighbour is owned by the same player as is
				// capturing this hex
				if (neighbour.getTerritory() != srcTerritory) {
					// the territories are different, so we need to
					// amalgamate them
					srcTerritory.amalgamate(neighbour.getTerritory());
				}
			}
		}
	}

	/**
	 * 
	 * @param graphics
	 */
	public final void draw(Graphics graphics) {
		Color colour = UNOWNED_LAND_COLOUR;
		if (NO_PLAYER != _owningPlayer) {
			colour = _board.getPlayer(_owningPlayer).getColour();
		} else if (!_isLand) {
			colour = SEA_COLOUR;
		}

		graphics.setColor(colour);

		graphics.fillPolygon(_xPts, _yPts, 6);
	}

	/**
	 * Draw either as unowned land of as water. This is for territory once seen
	 * by the human, but now out of sight.
	 * 
	 * @param graphics
	 */
	final void drawBasic(Graphics graphics) {
		Color colour = UNOWNED_LAND_COLOUR;
		if (!_isLand) {
			colour = SEA_COLOUR;
		}
		graphics.setColor(colour);
		graphics.fillPolygon(_xPts, _yPts, 6);
	}

	/**
	 * 
	 * @param graphics
	 */
	public void drawBorder(Graphics graphics) {
		if (_isLand) {
			for (int i = 0; i < _neighbours.length; i++) {
				Hexagon h = _neighbours[i];
				if (null == h) {
					graphics.setColor(Color.BLACK);
					graphics.drawLine(_xPts[i], _yPts[i], _xPts[i + 1], _yPts[i + 1]);
				} else {
					if (h.getOwner() == getOwner()) {
						graphics.setColor(Color.GRAY);
						graphics.drawLine(_xPts[i], _yPts[i], _xPts[i + 1], _yPts[i + 1]);
					} else {
						graphics.setColor(Color.BLACK);
						graphics.drawLine(_xPts[i], _yPts[i], _xPts[i + 1], _yPts[i + 1]);
					}
				}
			}
		}
	}

	public final void drawOccupant(Graphics graphics, boolean flashOn) {
		int type = getType(_occupant);
		switch (type) {
			case CASTLE:
				Glyphs.drawCastle(graphics, _x, _y, _radius);
				break;
			case TOWN:
				if (null != _territory) { // Why did we need this?
					boolean currentTerritory = _territory.isCurrent();
					if (!currentTerritory || flashOn) {
						int money = _territory.getMoney();
						Color colour = Color.LIGHT_GRAY;
						String str = null;
						boolean ownerPlaying = _board.getPlayer(_owningPlayer).isPlaying();
						if (ownerPlaying && money >= 10) {
							colour = new Color(255, 100, 100);
						}

						if (_board.getPlayer(_owningPlayer).isHuman()) { // REFACTOR
							str = Integer.toString(money);
						}
						Glyphs.drawTown(graphics, str, colour, _x, _y, _radius);
					}
				}
				break;

			case GRAVE:
				Glyphs.drawGrave(graphics, _x, _y, _radius);
				break;
			case CAPTAIN:
			case COLONEL:
			case MAJOR:
			case PRIVATE:
				boolean moveable = isMoveable(_occupant);
				if (flashOn || !_board.getPlayer(_owningPlayer).isHuman() || !moveable) { // REFACTOR
					Color colour = Glyphs.colour[type];
					Glyphs.drawSoldier(graphics, Color.BLACK, colour, _x, _y, _radius);
				}
				break;
			case NONE:
				// no-op
				break;
		}
	}

	/**
	 * @return the rank of the location. This is determined by the highest
	 *         ranking occupant or neighbouring occupant (of same player).
	 */
	public final int effectiveRank() {
		int rank = 0;
		if (isSoldier(_occupant) || isTown(_occupant) || isCastle(_occupant)) {
			// our occupants rank is a good starting point.
			// note that a town protects the hex it is on
			rank = rankOf[getType(_occupant)];
		}
		for (int i = 0; i < _neighbours.length; i++) {
			Hexagon neighbour = _neighbours[i];
			if (null != neighbour) {
				if (neighbour.getOwner() == _owningPlayer) {
					// only our own can protect us
					int occ = neighbour.getOccupant();
					if (NONE != occ) {
						if (isSoldier(occ) || isCastle(occ)) {
							int type = getType(occ);
							// towns do not protect their neighbours
							if (Settings.cumulativeDefence) {
								// System.out.println("Cumulative Defence");
								rank += rankOf[type];
							} else {
								int occRank = rankOf[type];
								if (occRank > rank) {
									rank = occRank;
								}
							}
						}
					}
				}
			}
		}

		return rank;
	}

	public final int getOccupant() {
		return _occupant;
	}

	public final int getOccupantType() {
		return getType(_occupant);
	}

	public final int getOwner() {
		return _owningPlayer;
	}

	/**
	 * @return Returns the playerData.
	 */
	public final Object getPlayerData() {
		return _playerData;
	}

	public final int getSeaNeighbourCount() {
		int count = 0;
		for (int i = 0; i < _neighbours.length; i++) {
			Hexagon neighbour = _neighbours[i];
			if (null != neighbour && neighbour.isSea()) {
				count = count + 1;
			}
		}
		return count;
	}

	final int getState() {
		int i = _occupant;
		if (isLand()) {
			i += LAND;
		}
		if (NO_PLAYER == _owningPlayer) {
			i += UNOWNED;
		} else {
			i += _owningPlayer << PLAYER_SHIFT;
		}
		return i;
	}

	public final Territory getTerritory() {
		return _territory;
	}

	public final boolean hasBeenSeenByHuman() {
		return _hasBeenSeenByHuman;
	}

	/**
	 * @param human
	 * @return
	 */
	public final boolean isVisibleToHuman(int human) {
		boolean isVisible = (_owningPlayer == human);
		for (int i = 0; i < _neighbours.length & !isVisible; i++) {
			Hexagon neighbour = _neighbours[i];
			if (null != neighbour) {
				isVisible = (neighbour.getOwner() == human);
			}
		}
		if (isVisible) {
			_hasBeenSeenByHuman = true;
		}
		return isVisible;
	}

	public final boolean occupantIsMovable() {
		return isMoveable(_occupant);
	}

	final void reset() {
		_territory = null;
		_owningPlayer = NO_PLAYER;
		// _isLand = false;
		_occupant = NONE;
		_hasBeenSeenByHuman = false;
	}

	public final void setOccupant(int occupant) {
		_occupant = occupant;
	}

	final void setOwner(int player) {
		_owningPlayer = player;
	}

	/**
	 * @param playerData
	 *            The playerData to set.
	 */
	public final void setPlayerData(Object playerData) {
		_playerData = playerData;
	}

	final void setState(int i, Player[] players) {
		_occupant = i & OCCUPANT_MASK;
		_isLand = ((i & LAND) == LAND);
		int p = i & PLAYER_MASK;
		if (p != UNOWNED) {
			p = p >> PLAYER_SHIFT;
			_owningPlayer = p;
		}
	}

	final void setTerritory(Territory territory) {
		_territory = territory;
	}

	/**
	 * @return
	 */
	public final String toShortString() {
		return "[" + _row + "," + _column + "]";
	}

	public final String toString() {
		return "[" + _row + "," + _column + "]";
	}

}
