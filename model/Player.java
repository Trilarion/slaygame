package model;

import gui.PlayerColours;

import java.awt.Color;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import model.algorithm.Attack;
import model.algorithm.AttackList;
import model.algorithm.DefaultAttackStrategy;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Player.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public final class Player implements PlayerColours {

	private static final int SPACE_FOR_EXPANSION = 10;
	private final DefaultAttackStrategy _attackStrategy = new DefaultAttackStrategy();
	private final Color _colour;
	private boolean _human = false;
	private final int _id;
	private boolean _inTheGame;
	private final String _name;

	private boolean _playing = false;

	public Player(int id, boolean human) {
		super();
		_name = "Player " + id;
		_colour = playerColour[id];
		_human = human;
		_id = id;
		int i = Random.positiveInteger(7) + 3;
		setTreeDepth(i);
		i = Random.positiveInteger(7) + 3;
		setTreeWidth(i);
		i = Random.positiveInteger(7) + 3;
		setCastleThreshold(i);
		System.out.println(this);
	}

	/**
	 * @param towns
	 */
	private void autoPlay(ArrayList territories) {
		System.out.println(getName() + " playing");
		for (int t = 0; t < territories.size(); t++) {
			Territory ter = (Territory) territories.get(t);
			_attackStrategy.prepareFor(ter);
			AttackList attackList = _attackStrategy.attack(ter);
			Iterator it = attackList.getAttacks();
			while (it.hasNext()) {
				Attack attack = (Attack) it.next();
				int soldier = attack.getSoldier();
				Hexagon hex = attack.getHexagon();
				ter.moveTo(soldier, hex);
			}
		}
	}

	/**
	 * @return Returns the castleThreshold.
	 */
	public final int getCastleThreshold() {
		return _attackStrategy.getCastleValue();
	}

	public final Color getColour() {
		return _colour;
	}

	/**
	 * @return
	 */
	public int getId() {
		return _id;
	}

	public final String getName() {
		return _name;
	}

	public final int getTimeout() {
		return (int) _attackStrategy.getTimeout();
	}

	/**
	 * @return Returns the treeDepth.
	 */
	public final int getTreeDepth() {
		return _attackStrategy.getTreeDepth();
	}

	/**
	 * @return Returns the treeWidth.
	 */
	public final int getTreeWidth() {
		return _attackStrategy.getTreeWidth();
	}

	public boolean isGuiPlayer() {
		return false;
	}

	/**
	 * @return Returns the human.
	 */
	public final boolean isHuman() {
		return _human;
	}

	/**
	 * @return Returns the inTheGame.
	 */
	public final boolean isInTheGame() {
		return _inTheGame;
	}

	public boolean isPlaying() {
		return _playing;
	}

	/**
	 * @param dis
	 * @throws IOException
	 */
	public void load(DataInputStream dis) throws IOException {
		// tree width
		// tree depth
		// timeout / 100
		// castle threshold
		// 

		int treeWidth = dis.readByte();
		int treeDepth = dis.readByte();
		int timeout = 100 * dis.readByte();
		int castleThreshold = dis.readByte();

		byte[] spaceForFuture = new byte[SPACE_FOR_EXPANSION];
		dis.read(spaceForFuture);

		setTreeWidth(treeWidth);
		setTreeDepth(treeDepth);
		setTimeout(timeout);
		setCastleThreshold(castleThreshold);

	}

	public void play(ArrayList territories) {
		autoPlay(territories);
	}

	/**
	 * @param dos
	 * @throws IOException
	 */
	public void save(DataOutputStream dos) throws IOException {
		// tree width
		// tree depth
		// timeout / 100
		// castle threshold
		dos.writeByte(getTreeWidth());
		dos.writeByte(getTreeDepth());
		dos.writeByte(getTimeout() / 100);
		dos.writeByte(getCastleThreshold());

		byte[] spaceForFuture = new byte[SPACE_FOR_EXPANSION];
		dos.write(spaceForFuture);
	}

	/**
	 * @param castleThreshold
	 *            The castleThreshold to set.
	 */
	public final void setCastleThreshold(int castleThreshold) {
		_attackStrategy.setCastleValue(castleThreshold);
	}

	/**
	 * @param human
	 *            The human to set.
	 */
	public final void setHuman(boolean human) {
		_human = human;
	}

	/**
	 * @param inTheGame
	 *            The inTheGame to set.
	 */
	public final void setInTheGame(boolean inTheGame) {
		_inTheGame = inTheGame;
	}

	public final void setPlaying(boolean playing) {
		_playing = playing;
	}

	public final void setTimeout(int t) {
		_attackStrategy.setTimeout(t);
	}

	/**
	 * @param treeDepth
	 *            The treeDepth to set.
	 */
	public final void setTreeDepth(int treeDepth) {
		_attackStrategy.setTreeDepth(treeDepth);
	}

	/**
	 * @param treeWidth
	 *            The treeWidth to set.
	 */
	public final void setTreeWidth(int treeWidth) {
		_attackStrategy.setTreeWidth(treeWidth);
	}

	public String toString() {
		return _name + " {w=" + getTreeWidth() + ", d=" + getTreeDepth() + ", c="
				+ getCastleThreshold() + ", t=" + getTimeout() + "}";
	}

}