import gui.SlayPanel;

import java.applet.Applet;
import java.awt.Dimension;
import java.awt.HeadlessException;

public class SlayApplet extends Applet {

	private static final long serialVersionUID = 1L;

	public SlayApplet() throws HeadlessException {
		super();
		SlayPanel slayPanel = new SlayPanel();
		add(slayPanel);
		slayPanel.setPreferredSize(new Dimension(800, 600));
	}

	public void resize(int arg0, int arg1) {
		super.resize(arg0, arg1);
		doLayout();
	}

}
