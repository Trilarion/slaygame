package gui;

import gui.assess.AssessmentPanel;

import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Panel;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

import model.Player;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Frame.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Top level GUI element in a Java Application
 */
public final class Frame extends java.awt.Frame {

	private static final long serialVersionUID = 1L;
	private static final String PLAY = "play";
	private static final String SETTINGS = "settings";
	private static final String ASSESMENT = "Assesment";

	private final FileManager _fileManager = new FileManager();
	private final SlayPanel _slayPanel;
	private final SettingsPanel _settings;
	private final AssessmentPanel _assesmentPanel;

	public static boolean develop = false;

	private final WindowAdapter _wa = new WindowAdapter() {
		public void windowClosing(WindowEvent e) {
			System.exit(0);
		}
	};

	private final CardLayout _cardLayout;
	private final Panel _cardPanel;

	public Frame() {
		super("Slay");
		addWindowListener(_wa);
		_cardLayout = new CardLayout();
		_cardPanel = new Panel(_cardLayout);
		add(_cardPanel);

		_slayPanel = new SlayPanel();
		_cardPanel.add(_slayPanel, PLAY);

		_settings = new SettingsPanel(this);
		_cardPanel.add(_settings, SETTINGS);

		_assesmentPanel = new AssessmentPanel();
		_cardPanel.add(_assesmentPanel, ASSESMENT);

		gui.MenuBar menuBar = new gui.MenuBar(this);
		setMenuBar(menuBar);
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		setBounds(0, 0, dim.width, dim.height);
		showPlay();
	}

	public final void editPlayers() {
		Player[] players = _slayPanel.getPlayers();
		if (null != players) {
			_settings.setup(players);
			_cardLayout.show(_cardPanel, SETTINGS);
		}
	}

	public final void showPlay() {
		_cardLayout.show(_cardPanel, PLAY);
	}

	public final void showAssesment() {
		_assesmentPanel.setGame(_slayPanel.getGame());
		_cardLayout.show(_cardPanel, ASSESMENT);
	}

	public final void loadGame() {
		try {
			File file = _fileManager.getLoadFile(this);
			_slayPanel.loadGame(file);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

	public final void saveGame() {
		try {
			File file = _fileManager.getSaveFile(this);
			_slayPanel.saveGame(file);
		} catch (IOException exception) {
			exception.printStackTrace();
		}
	}

}
