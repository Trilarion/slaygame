/**
 * 
 */
package gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import model.Player;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SettingsPanel.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
public class SettingsPanel extends Panel implements ActionListener {

	private static final long serialVersionUID = 1L;
	private static final String CANCEL = "Cancel";
	private static final String OK = "Ok";

	private PlayerGuiData[] _players;
	private Button cancelButton;
	private Button okButton;
	private final Frame _frame;

	public SettingsPanel(Frame frame) {
		super(new BorderLayout());
		_frame = frame;
		add(BorderLayout.SOUTH, createOkCancelPanel());
		okButton.setEnabled(true);
	}

	private Panel _dataPanel = null;

	public final void setup(Player[] players) {
		if (null != _dataPanel) {
			remove(_dataPanel);
		}
		_players = new PlayerGuiData[players.length];
		for (int i = 0; i < players.length; i++) {
			Player player = players[i];
			_players[i] = new PlayerGuiData(player);

		}

		GridLayout grid = new GridLayout(-1, 1, 30, 30);
		_dataPanel = new Panel(grid);
		_dataPanel.add(PlayerGuiData.getTitles());
		for (int i = 0; i < _players.length; i++) {
			PlayerGuiData player = _players[i];
			_dataPanel.add(player);

		}
		add(BorderLayout.NORTH, _dataPanel);
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if (OK.equals(cmd)) {
			for (int i = 0; i < _players.length; i++) {
				PlayerGuiData player = _players[i];
				player.setValues();
			}
			_frame.showPlay();
		} else if (CANCEL.equals(cmd)) {
			_frame.showPlay();
		}
	}

	/**
	 * AddQmDialogue constructor comment.
	 * 
	 * @param parent
	 *            java.awt.Frame
	 */
	public final Panel createOkCancelPanel() {
		Panel buttonPanel = new Panel(new FlowLayout(FlowLayout.CENTER));
		okButton = new Button(OK);
		okButton.addActionListener(this);
		buttonPanel.add(okButton);
		cancelButton = new Button(CANCEL);
		cancelButton.addActionListener(this);
		buttonPanel.add(cancelButton);
		return buttonPanel;
	}
}
