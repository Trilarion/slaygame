package gui;

import java.awt.CheckboxMenuItem;
import java.awt.HeadlessException;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import model.Random;
import model.Settings;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * MenuBar.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
final class MenuBar extends java.awt.MenuBar implements ActionListener, ItemListener {
	private static final long serialVersionUID = 1L;
	private static final String MI_EDIT_PLAYERS = "Edit Player Settings";
	private static final String MI_ASSESS = "Assess";
	private static final String MI_PLAY = "Play";
	private static final String MI_FILE_LOAD = "Load Game";
	private static final String MI_FILE_SAVE = "Save Game";
	private static final String MI_RUN_EXPERT = "Expert Mode";
	private static final String MI_CUMULATIVE_DEFENCE = "Cumulative Defence";
	private static final String MI_ALLOW_RETIRE = "Allow Retirement";
	private static final String MI_REMEMBER_MAP = "Remember Map";
	private static final String MI_TOWN_MOVE = "Allow Town Move";
	private static final String MI_RUN_RERANDOM = "Reseed Randomizer";
	private static final String MI_RUN_UNRANDOM = "Standardize Randomizer";

	private CheckboxMenuItem miExpertMode;
	private CheckboxMenuItem miAllowTownMove;
	private CheckboxMenuItem miAllowRetire;
	private CheckboxMenuItem miCumulativeDefence;
	private CheckboxMenuItem miRememberMap;

	private final Frame _frame;

	MenuBar(Frame frame) throws HeadlessException {
		super();
		_frame = frame;

		// /////////////////// File Menu ///////////////////////////////
		Menu menu = new Menu("File");
		MenuItem menuItem;
		menuItem = new MenuItem(MI_FILE_SAVE);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		menuItem = new MenuItem(MI_FILE_LOAD);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		if (Frame.develop) {
			menuItem = new MenuItem(MI_ASSESS);
			menuItem.addActionListener(this);
			menu.add(menuItem);

			menuItem = new MenuItem(MI_PLAY);
			menuItem.addActionListener(this);
			menu.add(menuItem);
		}

		add(menu);
		menu.add(menuItem);

		menu = new Menu("Settings");

		miExpertMode = new CheckboxMenuItem(MI_RUN_EXPERT);
		miExpertMode.addItemListener(this);
		miExpertMode.setState(Settings.expert);
		menu.add(miExpertMode);

		miAllowRetire = new CheckboxMenuItem(MI_ALLOW_RETIRE);
		miAllowRetire.addItemListener(this);
		miAllowRetire.setState(Settings.allowRetire);
		menu.add(miAllowRetire);

		miAllowTownMove = new CheckboxMenuItem(MI_TOWN_MOVE);
		miAllowTownMove.addItemListener(this);
		miAllowTownMove.setState(Settings.allowTownMove);
		menu.add(miAllowTownMove);

		miCumulativeDefence = new CheckboxMenuItem(MI_CUMULATIVE_DEFENCE);
		miCumulativeDefence.addItemListener(this);
		miCumulativeDefence.setState(Settings.cumulativeDefence);
		miCumulativeDefence.setEnabled(false);
		menu.add(miCumulativeDefence);

		miRememberMap = new CheckboxMenuItem(MI_REMEMBER_MAP);
		miRememberMap.addItemListener(this);
		miRememberMap.setState(Settings.rememberMap);
		menu.add(miRememberMap);

		menuItem = new MenuItem(MI_EDIT_PLAYERS);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		add(menu);

		menuItem = new MenuItem(MI_RUN_RERANDOM);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		menuItem = new MenuItem(MI_RUN_UNRANDOM);
		menuItem.addActionListener(this);
		menu.add(menuItem);

		add(menu);
	}

	public final void actionPerformed(ActionEvent event) {
		final String cmd = event.getActionCommand();
		if (MI_FILE_SAVE.equals(cmd)) {
			_frame.saveGame();
		} else if (MI_FILE_LOAD.equals(cmd)) {
			_frame.loadGame();
		} else if (MI_RUN_RERANDOM.equals(cmd)) {
			Random.reseed();
		} else if (MI_RUN_UNRANDOM.equals(cmd)) {
			Random.standardise();
		} else if (MI_EDIT_PLAYERS.equals(cmd)) {
			_frame.editPlayers();
		} else if (MI_ASSESS.equals(cmd)) {
			_frame.showAssesment();
		} else if (MI_PLAY.equals(cmd)) {
			_frame.showPlay();
		}
	}

	public final void itemStateChanged(ItemEvent e) {
		Settings.expert = miExpertMode.getState();
		Settings.allowRetire = miAllowRetire.getState();
		Settings.allowTownMove = miAllowTownMove.getState();
		Settings.cumulativeDefence = miCumulativeDefence.getState();
		Settings.rememberMap = miRememberMap.getState();
	}

}
