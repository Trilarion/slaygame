package gui;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import model.GeneralConstants;
import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PlayView.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * <ol>
 * <li>handles mouse input</li>
 * <li>provides drawing area for game</li>
 * </ol>
 */
public final class PlayView extends Panel implements MouseListener, GeneralConstants {

	private static final long serialVersionUID = 1L;

	/**
	 * convert mouse event into button .
	 */
	private static final boolean _isPopupTrigger(MouseEvent e) {
		return ((e.getModifiers() & MouseEvent.BUTTON3_MASK) == MouseEvent.BUTTON3_MASK)
				|| e.isPopupTrigger();
	}

	private String _message = "Select game from panel top right";

	private final Flasher _flasher;
	private final SlayPanel _slayPanel;
	private final BuyWindow _buyWindow;

	PlayView(SlayPanel slayPanel) {
		addMouseListener(this);
		_flasher = new Flasher(this);
		_flasher.start();
		_slayPanel = slayPanel;
		_buyWindow = new BuyWindow(this);
		add(_buyWindow);
		_buyWindow.setVisible(false);
	}

	public final void mouseClicked(MouseEvent event) {
	}

	public final void mouseEntered(MouseEvent arg0) {
	}

	public final void mouseExited(MouseEvent arg0) {
	}

	public final void mousePressed(MouseEvent event) {
		Point pt = event.getPoint();
		Territory territory = _slayPanel.getTerritory(pt);
		_buyWindow.setTerritory(territory);
		if (_isPopupTrigger(event)) {
			_buyWindow.setLocation(pt);
			if (!_buyWindow.isVisible()) {
				_buyWindow.setVisible(true);
			}
		} else {
			Cursor cursor = _slayPanel.pickUp(pt);
			if (null != cursor) {
				setCursor(cursor);
			}
			repaint();
		}
	}

	public final void mouseReleased(MouseEvent event) {
		Point pt = event.getPoint();
		if (_slayPanel.putDown(pt)) {
			setCursor(Cursor.getDefaultCursor());
			Territory territory = _slayPanel.getTerritory(pt);
			_buyWindow.setTerritory(territory);
		}
		repaint();
	}

	public final void paint(Graphics graphics) {
		Dimension dim = getSize();
		Color background = Color.WHITE;
		graphics.setColor(background);
		graphics.fillRect(0, 0, dim.width, dim.height);
		if (null == _message) {
			_slayPanel.draw(graphics, true);
			_buyWindow.refresh();
		} else {
			_slayPanel.draw(graphics, true);
			_buyWindow.setVisible(false);
			FontMetrics fm = graphics.getFontMetrics();
			int txtWidth = fm.stringWidth(_message);
			int txtHeight = fm.getHeight() + fm.getAscent();
			Image img = createImage(txtWidth, txtHeight);
			Graphics imGraphics = img.getGraphics();
			imGraphics.setColor(Color.BLACK);
			imGraphics.drawString(_message, 0, fm.getHeight());

			int h = (txtHeight * dim.width) / dim.height;
			int y = (dim.height - txtHeight) / 2;

			graphics.drawImage(img, 0, y, dim.width, h, null);
		}
	}

	final void paintFlash(boolean flash) {
		Graphics graphics = getGraphics();
		if (null == graphics) {
			return;
		}
		if (null == _message) {
			_slayPanel.draw(graphics, flash);
		}
	}

	/**
	 * @param pt
	 *            absolute coordinates
	 */
	final Point relativeCoordinates(Point pt) {
		Point ptScreen = getLocationOnScreen();
		pt.x -= ptScreen.x;
		pt.y -= ptScreen.y;
		return pt;
	}

	public final void update(Graphics arg0) {
		paint(arg0);
	}

	final SlayPanel getSlayPanel() {
		return _slayPanel;
	}

	public final void setMessage(String message) {
		_message = message;
		repaint();
	}

}
