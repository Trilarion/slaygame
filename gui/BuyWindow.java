package gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import model.GeneralConstants;
import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * BuyWindow.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
final class BuyWindow extends Panel implements MouseListener, MouseMotionListener, GeneralConstants {
	private static final long serialVersionUID = 1L;

	private Point _lastMouseDrag = null;

	private final MoveBar _moveBar;
	private final Shop _shop;

	BuyWindow(PlayView playView) {
		super(new BorderLayout());
		setSize(150, 75);
		setPreferredSize(new Dimension(150, 75));
		_shop = new Shop(playView.getSlayPanel(), null);
		add(_shop, BorderLayout.CENTER);
		_moveBar = new MoveBar(this);
		_moveBar.setPreferredSize(new Dimension(-1, 16));
		_moveBar.setBackground(Color.BLUE);
		add(_moveBar, BorderLayout.NORTH);
		_moveBar.addMouseListener(this);
		_moveBar.addMouseMotionListener(this);
		doLayout();
	}

	final void close() {
		setVisible(false);
	}

	public final void mouseClicked(MouseEvent arg0) {
	}

	public final void mouseDragged(MouseEvent event) {
		if (null != _lastMouseDrag) {
			Point pt = event.getPoint();
			int dx = pt.x - _lastMouseDrag.x;
			int dy = pt.y - _lastMouseDrag.y;
			pt = getLocation();
			pt.x += dx;
			pt.y += dy;
			setLocation(pt);
		}
	}

	public final void mouseEntered(MouseEvent arg0) {
	}

	public final void mouseExited(MouseEvent arg0) {
	}

	public final void mouseMoved(MouseEvent arg0) {
	}

	public final void mousePressed(MouseEvent event) {
		_lastMouseDrag = event.getPoint();
	}

	public final void mouseReleased(MouseEvent event) {
		_lastMouseDrag = null;
	}

	public final void paint(Graphics graphics) {
		Dimension dim = getSize();

		graphics.setColor(Color.BLUE);
		graphics.fillRect(0, 0, dim.width, dim.height);

	}

	public void setVisible(boolean show) {
		if (show) {
			setSize(150, 75);
			doLayout();
		}
		super.setVisible(show);
	}

	final void setTerritory(Territory territory) {
		_shop.setTerritory(territory);
	}

	public void refresh() {
		_shop.repaint();
	}
}
