package gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Panel;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * RankingPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Provides a bar-chart of the relative areas held by each player
 */
class RankingPanel extends Panel implements PlayerColours {

	private static final long serialVersionUID = 1L;
	private int[] _rankings;

	RankingPanel() {
		super();
	}

	public final void paint(Graphics graphics) {
		if (null != _rankings) {

			int numberOfRankings = _rankings.length;
			int maxRank = 0;
			for (int i = 0; i < numberOfRankings; i++) {
				if (_rankings[i] > maxRank) {
					maxRank = _rankings[i];
				}
			}

			Dimension dim = getSize();
			int deltaX = dim.width / numberOfRankings;
			int x = 0;
			int height = dim.height - 1;
			for (int i = 0; i < numberOfRankings; i++) {
				graphics.setColor(playerColour[i]);
				int y = (_rankings[i] * height) / maxRank;
				// int y = (i * dim.height) / numberOfRankings;
				graphics.fillRect(x, height - y, deltaX, y);
				graphics.setColor(Color.BLACK);
				graphics.drawRect(x, height - y, deltaX, y);
				x += deltaX;
			}
			graphics.drawRect(0, 0, x, height);
		}
	}

	final void setRankings(int[] rankings) {
		_rankings = rankings;
		repaint();
	}
}
