package gui;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import model.Game;
import model.Player;
import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SlayPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * This is the top level Panel containing the essential code for the Slay game.
 * <p>
 * All GUI input is routed through this class to the Game instance it contains.
 * All GUI output from the game is routed through this class.
 * </p>
 */
public final class SlayPanel extends java.awt.Panel {

	private static final int LARGE_GAME_RADIUS = 16;
	private static final int MEDIUM_GAME_RADIUS = 24;
	private static final long serialVersionUID = 1L;
	private static final int SMALL_GAME_RADIUS = 32;

	private final DetailPanel _detailPanel;
	private Game _game = null;
	private final gui.PlayView _playView;

	public SlayPanel() {
		super(new BorderLayout());
		_detailPanel = new DetailPanel(this);
		add(_detailPanel, BorderLayout.EAST);
		_playView = new gui.PlayView(this);
		add(_playView, BorderLayout.CENTER);
	}

	private final void _newGame(int radius) {
		int diameter = radius * 2;
		int verticalSeparation = (3 * radius) / 2;
		Dimension size = _playView.getSize();
		size.width = (size.width / diameter) - 2;
		size.height = (size.height / verticalSeparation) - 2;
		_game = new Game(this, size.height, size.width, radius);
		_playView.setMessage(null);
	}

	/**
	 * @param town
	 * @param pt
	 *            absolute cordinates
	 */
	final void buyCaptain(Territory territory, Point pt) {
		pt = _playView.relativeCoordinates(pt);
		if (null != _game) {
			_game.buyCaptain(territory, pt);
			_playView.repaint();
		}
	}

	/**
	 * @param town
	 * @param pt
	 *            absolute cordinates
	 */
	final void buyCastle(Territory territory, Point pt) {
		pt = _playView.relativeCoordinates(pt);
		if (null != _game) {
			_game.buyCastle(territory, pt);
			_playView.repaint();
		}
	}

	/**
	 * @param town
	 * @param pt
	 *            absolute cordinates
	 */
	final void buyColonel(Territory territory, Point pt) {
		pt = _playView.relativeCoordinates(pt);
		if (null != _game) {
			_game.buyColonel(territory, pt);
			_playView.repaint();
		}
	}

	/**
	 * @param town
	 * @param pt
	 *            absolute cordinates
	 */
	final void buyMajor(Territory territory, Point pt) {
		pt = _playView.relativeCoordinates(pt);
		if (null != _game) {
			_game.buyMajor(territory, pt);
			_playView.repaint();
		}
	}

	/**
	 * @param town
	 * @param pt
	 *            absolute cordinates
	 */
	final void buyPrivate(Territory territory, Point pt) {
		pt = _playView.relativeCoordinates(pt);
		if (null != _game) {
			_game.buyPrivate(territory, pt);
			_playView.repaint();
		}
	}

	public final void congratulations(Player winner) {
		String message = winner.getName() + " has won a memorable victory!";
		_playView.setMessage(message);
	}

	/**
	 * 
	 */
	public final void disableGUI() {
		_detailPanel.disableGUI();
		setWaitCursor();
	}

	final void draw(Graphics graphics, boolean flash) {
		if (null != _game) {
			_game.draw(graphics, flash);
			_detailPanel.refresh();
		}
	}

	/**
	 * 
	 */
	public final void enableGui() {
		_detailPanel.enableGui();
		setDefaultCursor();
	}

	/**
	 * 
	 */
	final void endHumanTurn() {
		_game.endHumanTurn();
	}

	final Game getGame() {
		return _game;
	}

	final Player[] getPlayers() {
		return _game.getPlayers();
	}

	final Territory getTerritory(Point pt) {
		Territory territory = null;
		if (null != _game) {
			territory = _game.getTerritory(pt);
		}
		return territory;
	}

	final void loadGame(File file) throws IOException {
		if (null != file) {
			InputStream is = new FileInputStream(file);
			DataInputStream dis = new DataInputStream(is);
			_game = new Game(this, dis);
		}
		_playView.setMessage(null);
		repaintViews();
	}

	/**
	 * 
	 */
	final void newLargeGame() {
		_newGame(LARGE_GAME_RADIUS);
	}

	final void newMediumGame() {
		_newGame(MEDIUM_GAME_RADIUS);
	}

	final void newSmallGame() {
		_newGame(SMALL_GAME_RADIUS);
	}

	final Cursor pickUp(Point pt) {
		if (null != _game) {
			return _game.pickUp(pt);
		}
		return null;
	}

	final boolean putDown(Point pt) {
		boolean moved = false;
		if (null != _game) {
			moved = _game.putDown(pt);
		}
		return moved;
	}

	public final void repaintViews() {
		_playView.repaint();
	}

	final void restartGame() {
		_game.restart();
	}

	final void saveGame(File file) throws IOException {
		_game.save(file);
	}

	public final void setCurrentTerritory(Territory territory) {
		_detailPanel.setTerritory(territory);
	}

	final void setDefaultCursor() {
		Cursor cursor = Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR);
		setCursor(cursor);
	}

	public final void setMove(int moveNumber) {
		_detailPanel.setMove(moveNumber);
	}

	public final void setRankings(int[] rankings) {
		_detailPanel.setRankings(rankings);
	}

	final void setWaitCursor() {
		Cursor cursor = Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR);
		setCursor(cursor);
	}

}
