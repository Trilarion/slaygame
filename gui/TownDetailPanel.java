package gui;

import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Panel;

import model.Player;
import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * TownDetailPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Displays current town and player information
 */
final class TownDetailPanel extends Panel {
	private static final long serialVersionUID = 1L;
	private int _move = 0;

	private Territory _territory;

	TownDetailPanel() {
		super();
	}

	final int getMoney() {
		int money = 0;
		if (null != _territory) {
			money = _territory.getMoney();
		}
		return money;
	}

	public final void paint(Graphics graphics) {
		FontMetrics fm = graphics.getFontMetrics();
		int textHeight = fm.getHeight();
		int textPos = textHeight;
		String str = "Move: " + _move;
		graphics.drawString(str, textHeight, textPos);
		textPos += textHeight;
		str = "Owner: ";
		if (null != _territory) {
			Player owner = _territory.getOwner();
			if (null != owner) {
				str += owner.getName();
			}
		}
		graphics.drawString(str, textHeight, textPos);
		textPos += textHeight;

		str = "Money: ";
		if (null != _territory) {
			str += _territory.getMoney();
		}

		graphics.drawString(str, textHeight, textPos);
		textPos += textHeight;

		str = "Income: ";
		if (null != _territory) {
			str += _territory.size();
		}

		graphics.drawString(str, textHeight, textPos);
		textPos += textHeight;

		str = "Expenses: ";
		if (null != _territory) {
			str += _territory.getExpenses();
		}

		graphics.drawString(str, textHeight, textPos);
		textPos += textHeight;

	}

	/**
	 * @param moveNumber
	 */
	final void setMove(int moveNumber) {
		_move = moveNumber;
		repaint();
	}

	final void setTerritory(Territory territory) {
		_territory = territory;
		repaint();
	}

	final Territory getTerritory() {
		return _territory;
	}

	public void refresh() {
		repaint();
	}

}
