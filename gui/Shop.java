package gui;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import model.GeneralConstants;
import model.Glyphs;
import model.Territory;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * Shop.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
final class Shop extends Canvas implements MouseListener, GeneralConstants {
	private static final int MARGIN = 5;

	private static final long serialVersionUID = 1L;

	private static final int SOLDIER_SIZE = 24;

	private int _dragging = NONE;

	private final Rectangle[] _rectangle = new Rectangle[5];
	private final SlayPanel _slayPanel;
	private Territory _territory;

	Shop(SlayPanel slayPanel, Territory territory) {

		setSize(new Dimension(150, SOLDIER_SIZE + MARGIN + MARGIN));
		_slayPanel = slayPanel;
		_territory = territory;
		for (int i = 0; i < _rectangle.length; i++) {
			_rectangle[i] = new Rectangle();
		}
		addMouseListener(this);
	}

	public Dimension getPreferredSize() {
		return new Dimension(150, SOLDIER_SIZE + MARGIN + MARGIN);
	}

	public void mouseClicked(MouseEvent arg0) {
	}

	public void mouseEntered(MouseEvent arg0) {
	}

	public void mouseExited(MouseEvent arg0) {
	}

	public void mouseMoved(MouseEvent arg0) {
	}

	public void mousePressed(MouseEvent event) {
		Point pt = event.getPoint();

		_dragging = NONE;
		int money = 40; // _detailPanel.getMoney();
		for (int i = 0; NONE == _dragging && i < _rectangle.length; i++) {
			if (_rectangle[i].contains(pt)) {
				if (money >= costOf[i]) {
					_dragging = i;
					setCursor(Glyphs.cursor[i]);
				}
			}
		}
	}

	public void mouseReleased(MouseEvent event) {
		Point pt = event.getPoint();
		if (NONE != _dragging) {
			setCursor(Cursor.getDefaultCursor());
			Point ptScreen = getLocationOnScreen();
			pt.x += ptScreen.x;
			pt.y += ptScreen.y;
			switch (_dragging) {
				case PRIVATE:
					_slayPanel.buyPrivate(_territory, pt);
					break;
				case CAPTAIN:
					_slayPanel.buyCaptain(_territory, pt);
					break;
				case COLONEL:
					_slayPanel.buyColonel(_territory, pt);
					break;
				case MAJOR:
					_slayPanel.buyMajor(_territory, pt);
					break;
				case CASTLE:
					_slayPanel.buyCastle(_territory, pt);
					break;
			}
			_dragging = NONE;
		}
		repaint();
	}

	public final void paint(Graphics graphics) {
		Dimension dim = getSize();

		graphics.setColor(Color.LIGHT_GRAY);
		graphics.fillRect(0, 0, dim.width, dim.height);

		int deltaX = (dim.width - 10) / 5;
		int y = dim.height - SOLDIER_SIZE - MARGIN;
		int x = (deltaX / 2) + MARGIN;

		int money = 0;
		if (null != _territory) {
			money = _territory.getMoney();
		}
		Glyphs soldier = new Glyphs(x, y, SOLDIER_SIZE);

		int rectIndex = 0;

		_rectangle[rectIndex].setBounds(x - (SOLDIER_SIZE / 2), y - SOLDIER_SIZE, SOLDIER_SIZE,
				SOLDIER_SIZE + SOLDIER_SIZE);
		graphics.setColor(Color.WHITE);
		Rectangle r = _rectangle[rectIndex];
		graphics.fillRect(r.x, r.y, r.width, r.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(r.x, r.y, r.width, r.height);
		if (money >= costOf[rectIndex++]) {
			soldier.drawPrivate(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (SOLDIER_SIZE / 2), y - SOLDIER_SIZE, SOLDIER_SIZE,
				SOLDIER_SIZE + SOLDIER_SIZE);
		graphics.setColor(Color.WHITE);
		r = _rectangle[rectIndex];
		graphics.fillRect(r.x, r.y, r.width, r.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(r.x, r.y, r.width, r.height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawCaptain(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (SOLDIER_SIZE / 2), y - SOLDIER_SIZE, SOLDIER_SIZE,
				SOLDIER_SIZE + SOLDIER_SIZE);
		graphics.setColor(Color.WHITE);
		r = _rectangle[rectIndex];
		graphics.fillRect(r.x, r.y, r.width, r.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(r.x, r.y, r.width, r.height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawMajor(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (SOLDIER_SIZE / 2), y - SOLDIER_SIZE, SOLDIER_SIZE,
				SOLDIER_SIZE + SOLDIER_SIZE);
		graphics.setColor(Color.WHITE);
		r = _rectangle[rectIndex];
		graphics.fillRect(r.x, r.y, r.width, r.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(r.x, r.y, r.width, r.height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawColonel(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (SOLDIER_SIZE / 2), y - SOLDIER_SIZE, SOLDIER_SIZE,
				SOLDIER_SIZE + SOLDIER_SIZE);
		graphics.setColor(Color.WHITE);
		r = _rectangle[rectIndex];
		graphics.fillRect(r.x, r.y, r.width, r.height);
		graphics.setColor(Color.BLACK);
		graphics.drawRect(r.x, r.y, r.width, r.height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			Glyphs.drawCastle(graphics, x, y, SOLDIER_SIZE);
		} else {
			Glyphs.drawPaleCastle(graphics, x, y, SOLDIER_SIZE);
		}
	}

	public void setTerritory(Territory territory) {
		_territory = territory;
		repaint();
	}
}
