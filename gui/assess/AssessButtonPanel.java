package gui.assess;

import java.awt.GridLayout;
import java.awt.Panel;

public class AssessButtonPanel extends Panel {

	private static final long serialVersionUID = 1L;
	private final AssessDetail _detailPanel;

	public AssessButtonPanel(AssessDetail detailPanel) {
		super(new GridLayout(0, 2));
		_detailPanel = detailPanel;
		for (int i = 0; i < 6; i++) {
			PlayerButton button = new PlayerButton(this, i);
			add(button);
		}
	}

	public void setPlayer(int player) {
		_detailPanel.setPlayer(player);
	}

}
