package gui.assess;

import java.awt.Graphics;
import java.awt.Panel;

import model.algorithm2.PlayerAssessment;

public class AssessView extends Panel {

	private static final long serialVersionUID = 1L;
	// private final AssessmentPanel _panel;
	private PlayerAssessment _playerAssessment;

	public AssessView(AssessmentPanel panel) {
		super();
		// _panel = panel;
	}

	public void paint(Graphics graphics) {
		if (null == _playerAssessment) {
			return;
		}
		// _playerAssessment.draw(graphics);
	}

	public void setPlayerAssessment(PlayerAssessment pAssess) {
		_playerAssessment = pAssess;
	}

}
