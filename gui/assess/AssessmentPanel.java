package gui.assess;

import java.awt.BorderLayout;
import java.awt.Panel;

import model.Game;
import model.algorithm2.PlayerAssessment;

public class AssessmentPanel extends Panel {
	private static final long serialVersionUID = -1L;

	private final AssessDetail _detail;
	private Game _game = null;
	private final AssessView _view;

	public AssessmentPanel() {
		super(new BorderLayout());

		_view = new AssessView(this);
		_detail = new AssessDetail(this);
		add(_view, BorderLayout.CENTER);
		add(_detail, BorderLayout.EAST);

	}

	public final Game getGame() {
		return _game;
	}

	public final void setGame(Game game) {
		if (null != game) {
			_game = game;
			// _planBoard = _game.getPlan();
			setPlayer(0);
		}
	}

	private int currentId = -1;
	private PlayerAssessment pAssess = null;

	public void setPlayer(int playerId) {
		if (null != _game) {
			// _planBoard.resetPlan();
			// if (playerId != currentId) {
			// Player player = _game.getPlayers()[playerId];
			// pAssess = new PlayerAssessment(player, _planBoard);
			// _view.setPlayerAssessment(pAssess);
			// }
			// pAssess.calculate();
			_view.repaint();
		}
	}
}
