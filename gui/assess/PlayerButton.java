package gui.assess;

import java.awt.Button;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public final class PlayerButton extends Button implements ActionListener {

	private static final long serialVersionUID = 1L;
	private final AssessButtonPanel _buttonPanel;
	private final int _player;

	public PlayerButton(AssessButtonPanel buttonPanel, int player) throws HeadlessException {
		super("Player " + player);
		_player = player;
		_buttonPanel = buttonPanel;
		addActionListener(this);
	}

	public final void actionPerformed(ActionEvent arg0) {
		_buttonPanel.setPlayer(_player);
	}

}
