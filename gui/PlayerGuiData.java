/**
 * 
 */
package gui;

import java.awt.Checkbox;
import java.awt.GridLayout;
import java.awt.Label;
import java.awt.Panel;

import model.Player;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * PlayerGuiData.java is part of Phill van Leersum's SlayGame program.
 * 
 * SlayGame is free software: you can redistribute it and/or modify it under the
 * terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * SlayGame is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * SlayGame. If not, see http://www.gnu.org/licenses/.
 * 
 */
/**
 * 
 */
public class PlayerGuiData extends Panel {

	private static final String[] _titles = new String[] { "Player", "Human", "Tree Width",
			"Tree Depth", "Castle Value", "TimeOut" };

	public static final Panel getTitles() {
		Panel panel = new Panel(new GridLayout(1, -1, 100, 100));
		for (int i = 0; i < _titles.length; i++) {
			String str = _titles[i];
			panel.add(new Label(str, Label.CENTER));
		}
		return panel;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private final Player _player;
	private final SpinControl _treeWidth, _treeDepth, _castleThreshold, _timeout;
	private final Checkbox _human;

	/**
	 * 
	 */
	public PlayerGuiData(Player player) {
		super(new GridLayout(1, -1, 100, 100));

		_player = player;
		add(new Label(_player.getName(), Label.CENTER));
		_human = new Checkbox("", player.isHuman());
		_human.setEnabled(false);
		Panel panel = new Panel();
		panel.add(_human);
		add(panel);
		_treeWidth = new SpinControl(player.getTreeWidth(), 1, 10);
		add(_treeWidth);
		_treeDepth = new SpinControl(player.getTreeDepth(), 1, 10);
		add(_treeDepth);
		_castleThreshold = new SpinControl(player.getCastleThreshold(), 1, 10);
		add(_castleThreshold);
		_timeout = new SpinControl(player.getTimeout() / 100, 1, 100);
		add(_timeout);
	}

	/**
	 * 
	 */
	public void setValues() {
		_player.setHuman(_human.getState());
		_player.setCastleThreshold(_castleThreshold.getValue());
		_player.setTreeDepth(_treeDepth.getValue());
		_player.setTreeWidth(_treeWidth.getValue());
		_player.setTimeout(_timeout.getValue() * 100);
	}

}
