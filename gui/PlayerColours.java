package gui;

import java.awt.Color;

public interface PlayerColours {
	public static final Color[] playerColour = new Color[] { new Color(120, 255, 120),
			new Color(255, 255, 160), new Color(200, 200, 120), new Color(160, 255, 160),
			new Color(255, 180, 180), new Color(220, 220, 160), };

}
