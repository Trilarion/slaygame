package gui;

import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Label;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SpinControl.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * 
 */
public class SpinControl extends Panel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private static final String _INCREMENT = ">>";

	private static final String _DECREMENT = "<<";

	private int _maximum, _minimum, _current, _increment;

	private Label _value;

	/**
	 * SpinControl constructor comment.
	 */
	public SpinControl(int initial, int minimum, int maximum) {
		this(initial, minimum, maximum, 1);
	}

	/**
	 * SpinControl constructor comment.
	 */
	public SpinControl(int initial, int minimum, int maximum, int increment) {
		super(new BorderLayout());
		_current = initial;
		_maximum = maximum;
		_minimum = minimum;
		_increment = increment;

		Button b = new Button(_DECREMENT);
		b.addActionListener(this);
		add(BorderLayout.WEST, b);
		b = new Button(_INCREMENT);
		b.addActionListener(this);
		add(BorderLayout.EAST, b);
		_value = new Label(Integer.toString(maximum) + "    ");
		_value.setAlignment(Label.CENTER);
		_value.setText(Integer.toString(_current));
		add(BorderLayout.CENTER, _value);
	}

	public void actionPerformed(ActionEvent e) {
		String cmd = e.getActionCommand();
		if (_INCREMENT.equals(cmd)) {
			if (_current < _maximum) {
				_current += _increment;
				if (_current > _maximum) {
					_current = _maximum;
				}
				_value.setText(Integer.toString(_current));
			}
		} else if (_DECREMENT.equals(cmd)) {
			if (_current > _minimum) {
				_current -= _increment;
				if (_current < _minimum) {
					_current = _minimum;
				}
				_value.setText(Integer.toString(_current));
			}
		}
	}

	public final int getValue() {
		return _current;
	}
}
