package gui;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Panel;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;

import model.GeneralConstants;
import model.Glyphs;

/*
 * Copyright 2010 Phill van Leersum
 * 
 * SoldierPanel.java is part of Phill van Leersum's SlayGame program.
 *
 * SlayGame is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * SlayGame is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with SlayGame.  If not, see http://www.gnu.org/licenses/.
 *
 */
/**
 * Displays the soldier colours and pay rates.
 */
final class SoldierPanel extends Panel implements MouseListener, MouseMotionListener,
		GeneralConstants {

	private static final long serialVersionUID = 1L;

	private final DetailPanel _detailPanel;
	private int _dragging = NONE;
	private final Rectangle[] _rectangle = new Rectangle[5];

	SoldierPanel(DetailPanel detailPanel) {
		super();
		_detailPanel = detailPanel;
		addMouseListener(this);
		for (int i = 0; i < _rectangle.length; i++) {
			_rectangle[i] = new Rectangle();
		}
	}

	public final void mouseClicked(MouseEvent arg0) {
	}

	public void mouseDragged(MouseEvent event) {
	}

	public final void mouseEntered(MouseEvent arg0) {
	}

	public final void mouseExited(MouseEvent arg0) {
	}

	public void mouseMoved(MouseEvent arg0) {
	}

	public final void mousePressed(MouseEvent event) {
		Point pt = event.getPoint();
		_dragging = NONE;
		int money = _detailPanel.getMoney();
		for (int i = 0; NONE == _dragging && i < _rectangle.length; i++) {
			if (_rectangle[i].contains(pt)) {
				if (money >= costOf[i]) {
					_dragging = i;
					setCursor(Glyphs.cursor[i]);
				}
			}
		}
	}

	public final void mouseReleased(MouseEvent event) {
		Point pt = event.getPoint();
		if (NONE != _dragging) {
			setCursor(Cursor.getDefaultCursor());
			Point ptScreen = getLocationOnScreen();
			pt.x += ptScreen.x;
			pt.y += ptScreen.y;
			switch (_dragging) {
				case PRIVATE:
					_detailPanel.buyPrivate(pt);
					break;
				case CAPTAIN:
					_detailPanel.buyCaptain(pt);
					break;
				case COLONEL:
					_detailPanel.buyColonel(pt);
					break;
				case MAJOR:
					_detailPanel.buyMajor(pt);
					break;
				case CASTLE:
					_detailPanel.buyCastle(pt);
					break;
			}
			_dragging = NONE;
			repaint();
		}
	}

	public final void paint(Graphics graphics) {
		Dimension dim = getSize();

		String str = "Pay Scale";
		FontMetrics fm = graphics.getFontMetrics();
		int strWidth = fm.stringWidth(str);
		int textX = (dim.width - strWidth) / 2;
		int textY = fm.getAscent() + fm.getHeight();
		graphics.drawString(str, textX, textY);

		int deltaX = dim.width / 5;
		textY += fm.getAscent() + fm.getHeight();
		int y = dim.height / 2;
		int x = dim.width / 8;
		int size = 24;

		int money = _detailPanel.getMoney();
		Glyphs soldier = new Glyphs(x, y, size);

		int rectIndex = 0;

		_rectangle[rectIndex].setBounds(x - (size / 2), y - size, size, size + size);
		graphics.drawRect(_rectangle[rectIndex].x, _rectangle[rectIndex].y,
				_rectangle[rectIndex].width, _rectangle[rectIndex].height);
		if (money >= costOf[rectIndex++]) {
			soldier.drawPrivate(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		str = "= 2";
		strWidth = fm.stringWidth(str);
		textX = x - (strWidth / 2);
		graphics.drawString(str, textX, textY);

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (size / 2), y - size, size, size + size);
		graphics.drawRect(_rectangle[rectIndex].x, _rectangle[rectIndex].y,
				_rectangle[rectIndex].width, _rectangle[rectIndex].height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawCaptain(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		str = "= 6";
		strWidth = fm.stringWidth(str);
		textX = x - (strWidth / 2);
		graphics.drawString(str, textX, textY);

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (size / 2), y - size, size, size + size);
		graphics.drawRect(_rectangle[rectIndex].x, _rectangle[rectIndex].y,
				_rectangle[rectIndex].width, _rectangle[rectIndex].height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawMajor(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		str = "= 18";
		strWidth = fm.stringWidth(str);
		textX = x - (strWidth / 2);
		graphics.drawString(str, textX, textY);

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (size / 2), y - size, size, size + size);
		graphics.drawRect(_rectangle[rectIndex].x, _rectangle[rectIndex].y,
				_rectangle[rectIndex].width, _rectangle[rectIndex].height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			soldier.drawColonel(graphics);
		} else {
			soldier.drawPaleSoldier(graphics);
		}

		str = "= 54";
		strWidth = fm.stringWidth(str);
		textX = x - (strWidth / 2);
		graphics.drawString(str, textX, textY);

		x += deltaX;
		_rectangle[rectIndex].setBounds(x - (size / 2), y - size, size, size + size);
		graphics.drawRect(_rectangle[rectIndex].x, _rectangle[rectIndex].y,
				_rectangle[rectIndex].width, _rectangle[rectIndex].height);
		soldier.x = x;
		if (money >= costOf[rectIndex++]) {
			Glyphs.drawCastle(graphics, x, y, size);
		} else {
			Glyphs.drawPaleCastle(graphics, x, y, size);
		}

		str = "= 0";
		strWidth = fm.stringWidth(str);
		textX = x - (strWidth / 2);
		graphics.drawString(str, textX, textY);
	}

	public void refresh() {
		repaint();
	}
}
